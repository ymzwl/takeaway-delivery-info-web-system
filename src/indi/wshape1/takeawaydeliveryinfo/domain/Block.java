package indi.wshape1.takeawaydeliveryinfo.domain;

/**
 * @author Wshape1
 * @create 2022-11-11 12:40
 */

public class Block {
    private String bl_name;
    private String info = null;

    public Block() {
    }

    public Block(String bl_name) {
        this.bl_name = bl_name;
    }

    public String getName() {
        return bl_name;
    }

    public void getName(String bl_name) {
        this.bl_name = bl_name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Group[] getGroups() {

        return null;
    }

}
