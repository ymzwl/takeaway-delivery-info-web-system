package indi.wshape1.takeawaydeliveryinfo.dao;

import indi.wshape1.takeawaydeliveryinfo.domain.Group;

import java.util.List;

/**
 * @author Wshape1
 * @create 2022-11-11 16:38
 */

public interface GroupDAO extends RangeDAO<Group> {

    int countPerPage = 100;

    default int getCountPerPage() {
        return countPerPage;
    }

    List<Group> getListByBlock(String block);

    List<Group> getListByBlockByKeywordAndPage(String block, String keyword, int page);

    int getCountByBlock(String block);
}
