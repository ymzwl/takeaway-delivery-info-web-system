/*
 Navicat Premium Data Transfer

 Source Server         : test
 Source Server Type    : MySQL
 Source Server Version : 80012 (8.0.12)
 Source Host           : localhost:3306
 Source Schema         : take_out

 Target Server Type    : MySQL
 Target Server Version : 80012 (8.0.12)
 File Encoding         : 65001

 Date: 12/12/2022 19:18:35
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ad_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '管理员',
  `phone` char(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `block` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `adminForBlock`(`block` ASC) USING BTREE,
  INDEX `phone`(`phone` ASC) USING BTREE,
  CONSTRAINT `adminForBlock` FOREIGN KEY (`block`) REFERENCES `block` (`bl_name`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES (6, '小神', '12345678900', '12345678900', '神马学院');
INSERT INTO `admin` VALUES (7, '小马', '12345678911', '12345678911', '神马学院');
INSERT INTO `admin` VALUES (8, '小菜', '98765432100', '98765432100', '菜徐村');
INSERT INTO `admin` VALUES (9, '小徐', '98765432111', '98765432111', '菜徐村');

-- ----------------------------
-- Table structure for block
-- ----------------------------
DROP TABLE IF EXISTS `block`;
CREATE TABLE `block`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bl_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `info` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `name`(`bl_name` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of block
-- ----------------------------
INSERT INTO `block` VALUES (3, 'null', NULL);
INSERT INTO `block` VALUES (7, '神马学院', '神马学院');
INSERT INTO `block` VALUES (8, '菜徐村', '菜徐村');

-- ----------------------------
-- Table structure for group
-- ----------------------------
DROP TABLE IF EXISTS `group`;
CREATE TABLE `group`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gr_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `info` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `block` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `name`(`gr_name` ASC) USING BTREE,
  INDEX `name_2`(`gr_name` ASC, `block` ASC) USING BTREE,
  INDEX `groupInBlock`(`block` ASC) USING BTREE,
  CONSTRAINT `groupInBlock` FOREIGN KEY (`block`) REFERENCES `block` (`bl_name`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of group
-- ----------------------------
INSERT INTO `group` VALUES (3, 'null', NULL, 'null');
INSERT INTO `group` VALUES (8, '南一', NULL, '神马学院');
INSERT INTO `group` VALUES (9, '南六', NULL, '神马学院');
INSERT INTO `group` VALUES (10, '南七', NULL, '神马学院');
INSERT INTO `group` VALUES (11, '中11', NULL, '神马学院');
INSERT INTO `group` VALUES (12, '北苑', NULL, '神马学院');
INSERT INTO `group` VALUES (13, '村头', NULL, '菜徐村');
INSERT INTO `group` VALUES (14, '村中', NULL, '菜徐村');
INSERT INTO `group` VALUES (15, '村尾', NULL, '菜徐村');

-- ----------------------------
-- Table structure for root
-- ----------------------------
DROP TABLE IF EXISTS `root`;
CREATE TABLE `root`  (
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of root
-- ----------------------------
INSERT INTO `root` VALUES ('root', 'rootrootroot');

-- ----------------------------
-- Table structure for sender
-- ----------------------------
DROP TABLE IF EXISTS `sender`;
CREATE TABLE `sender`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `se_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '配送员',
  `phone` char(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `group` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `block` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `senderInGroupBlock`(`group` ASC, `block` ASC) USING BTREE,
  INDEX `phone`(`phone` ASC) USING BTREE,
  CONSTRAINT `senderInGroupBlock` FOREIGN KEY (`group`, `block`) REFERENCES `group` (`gr_name`, `block`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sender
-- ----------------------------
INSERT INTO `sender` VALUES (7, '小一', '12345611111', '12345611111', '南一', '神马学院');
INSERT INTO `sender` VALUES (8, '小六', '12345666666', '12345666666', '南六', '神马学院');
INSERT INTO `sender` VALUES (9, '小七', '12345677777', '12345677777', '南七', '神马学院');
INSERT INTO `sender` VALUES (10, '小中', '12345671111', '12345671111', '中11', '神马学院');
INSERT INTO `sender` VALUES (11, '小北', '12345600000', '12345600000', '北苑', '神马学院');
INSERT INTO `sender` VALUES (12, '大头', '98765432199', '98765432199', '村头', '菜徐村');
INSERT INTO `sender` VALUES (13, '大中', '98765432188', '98765432188', '村中', '菜徐村');
INSERT INTO `sender` VALUES (14, '大尾', '98765432177', '98765432177', '村尾', '菜徐村');

-- ----------------------------
-- Table structure for takeout_info
-- ----------------------------
DROP TABLE IF EXISTS `takeout_info`;
CREATE TABLE `takeout_info`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` char(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` datetime NOT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '配送中',
  `group` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `block` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sender_phone` char(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `detail` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `infoGroupBlock`(`group` ASC, `block` ASC) USING BTREE,
  INDEX `inInfoSenderPhone`(`sender_phone` ASC) USING BTREE,
  INDEX `phone`(`phone` ASC) USING BTREE,
  CONSTRAINT `infoGroupBlock` FOREIGN KEY (`group`, `block`) REFERENCES `group` (`gr_name`, `block`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of takeout_info
-- ----------------------------
INSERT INTO `takeout_info` VALUES (1, 'A', '12312312300', '2022-12-09 20:53:12', '配送中', '南一', '神马学院', '12345611111', '碳烤鸭腿饭');
INSERT INTO `takeout_info` VALUES (2, 'A', '12312312300', '2022-12-09 20:52:44', '配送中', '南一', '神马学院', '12345611111', NULL);
INSERT INTO `takeout_info` VALUES (3, 'B', '12312312311', '2022-12-09 20:54:36', '配送中', '南七', '神马学院', '12345677777', '酸汤肥牛');
INSERT INTO `takeout_info` VALUES (4, 'B', '12312312311', '2022-12-09 20:54:36', '配送中', '南七', '神马学院', '12345677777', NULL);
INSERT INTO `takeout_info` VALUES (5, 'C', '12312312322', '2022-12-09 20:56:21', '已送达', '村头', '菜徐村', '98765432199', '宫保鸡丁');

-- ----------------------------
-- Procedure structure for getAdminListByBlockByKeywordAndPage
-- ----------------------------
DROP PROCEDURE IF EXISTS `getAdminListByBlockByKeywordAndPage`;
delimiter ;;
CREATE PROCEDURE `getAdminListByBlockByKeywordAndPage`(IN `in_block` VARCHAR(255), IN `keyword` varchar(255), IN `pageS` INT(11), IN `pageE` INT(11))
BEGIN
	SELECT * FROM
				((SELECT `ad_name`, `phone`, `password`, `block` FROM `admin` WHERE `ad_name` LIKE `keyword`)
			UNION
				(SELECT `ad_name`, `phone`, `password`, `block` FROM `admin` WHERE `phone` LIKE `keyword`) 
			UNION
				(SELECT `ad_name`, `phone`, `password`, `block` FROM `admin` WHERE `block` LIKE `keyword`)) AS t
		WHERE `block` = `in_block` LIMIT `pageS`, `pageE`;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for getSenderListByBlockByKeywordAndPage
-- ----------------------------
DROP PROCEDURE IF EXISTS `getSenderListByBlockByKeywordAndPage`;
delimiter ;;
CREATE PROCEDURE `getSenderListByBlockByKeywordAndPage`(IN `in_block` VARCHAR(255), IN `keyword` varchar(255), IN `pageS` INT(11), IN `pageE` INT(11))
BEGIN
	SELECT * FROM
				((SELECT `se_name`, `phone`, `password`, `group`, `block` FROM `sender` WHERE `se_name` LIKE `keyword`)
			UNION
				(SELECT `se_name`, `phone`, `password`, `group`, `block` FROM `sender` WHERE `phone` LIKE `keyword`) 
			UNION
				(SELECT `se_name`, `phone`, `password`, `group`, `block` FROM `sender` WHERE `group` LIKE `keyword`)
				UNION
				(SELECT `se_name`, `phone`, `password`, `group`, `block` FROM `sender` WHERE `block` LIKE `keyword`)) AS t
		WHERE `block` = `in_block` LIMIT `pageS`, `pageE`;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for getSenderListByGroupAndBlockByKeywordAndPage
-- ----------------------------
DROP PROCEDURE IF EXISTS `getSenderListByGroupAndBlockByKeywordAndPage`;
delimiter ;;
CREATE PROCEDURE `getSenderListByGroupAndBlockByKeywordAndPage`(IN `in_group` VARCHAR(255), IN `in_block` VARCHAR(255), IN `keyword` varchar(255), IN `pageS` INT(11), IN `pageE` INT(11))
BEGIN
		SELECT * FROM
				((SELECT `se_name`, `phone`, `password`, `group`, `block` FROM `sender` WHERE `se_name` LIKE `keyword`)
			UNION
				(SELECT `se_name`, `phone`, `password`, `group`, `block` FROM `sender` WHERE `phone` LIKE `keyword`) 
			UNION
				(SELECT `se_name`, `phone`, `password`, `group`, `block` FROM `sender` WHERE `group` LIKE `keyword`)
				UNION
				(SELECT `se_name`, `phone`, `password`, `group`, `block` FROM `sender` WHERE `block` LIKE `keyword`)) AS t
		WHERE `group` = `in_group` AND `block` = `in_block` LIMIT `pageS`, `pageE`;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for getTakeOutListByBlockByKeywordAndPage
-- ----------------------------
DROP PROCEDURE IF EXISTS `getTakeOutListByBlockByKeywordAndPage`;
delimiter ;;
CREATE PROCEDURE `getTakeOutListByBlockByKeywordAndPage`(IN `in_block` VARCHAR(255), IN `keyword` varchar(255), IN `pageS` INT(11), IN `pageE` INT(11))
BEGIN
	SELECT * FROM
				((SELECT `id`, `name`, `phone`, `time`, `status`, `group`, `block`, `sender_phone`, `detail` FROM `takeout_info` WHERE `name` LIKE `keyword`)
			UNION
				(SELECT `id`, `name`, `phone`, `time`, `status`, `group`, `block`, `sender_phone`, `detail` FROM `takeout_info` WHERE `phone` LIKE `keyword`) 
			UNION
				(SELECT `id`, `name`, `phone`, `time`, `status`, `group`, `block`, `sender_phone`, `detail` FROM `takeout_info` WHERE `sender_phone` LIKE `keyword`)) AS t
		WHERE `block` = `in_block` LIMIT `pageS`, `pageE`;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for getTakeOutListByGroupAndBlockByKeywordAndPage
-- ----------------------------
DROP PROCEDURE IF EXISTS `getTakeOutListByGroupAndBlockByKeywordAndPage`;
delimiter ;;
CREATE PROCEDURE `getTakeOutListByGroupAndBlockByKeywordAndPage`(IN `in_group` VARCHAR(255), IN `in_block` VARCHAR(255), IN `keyword` varchar(255), IN `pageS` INT(11), IN `pageE` INT(11))
BEGIN
		SELECT * FROM
				((SELECT `id`, `name`, `phone`, `time`, `status`, `group`, `block`, `sender_phone`, `detail` FROM `takeout_info` WHERE `name` LIKE `keyword`)
			UNION
				(SELECT `id`, `name`, `phone`, `time`, `status`, `group`, `block`, `sender_phone`, `detail` FROM `takeout_info` WHERE `phone` LIKE `keyword`) 
			UNION
				(SELECT `id`, `name`, `phone`, `time`, `status`, `group`, `block`, `sender_phone`, `detail` FROM `takeout_info` WHERE `sender_phone` LIKE `keyword`)) AS t
		WHERE `group` = `in_group` AND `block` = `in_block` LIMIT `pageS`, `pageE`;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for getTakeOutListByKeywordAndPage
-- ----------------------------
DROP PROCEDURE IF EXISTS `getTakeOutListByKeywordAndPage`;
delimiter ;;
CREATE PROCEDURE `getTakeOutListByKeywordAndPage`(IN `keyword` varchar(255), IN `pageS` INT(11), IN `pageE` INT(11))
BEGIN
	SELECT * FROM
				((SELECT `id`, `name`, `phone`, `time`, `status`, `group`, `block`, `sender_phone`, `detail` FROM `takeout_info` WHERE `name` LIKE `keyword`)
			UNION
				(SELECT `id`, `name`, `phone`, `time`, `status`, `group`, `block`, `sender_phone`, `detail` FROM `takeout_info` WHERE `phone` LIKE `keyword`) 
			UNION
				(SELECT `id`, `name`, `phone`, `time`, `status`, `group`, `block`, `sender_phone`, `detail` FROM `takeout_info` WHERE `sender_phone` LIKE `keyword`)) AS t
		LIMIT `pageS`, `pageE`;
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
