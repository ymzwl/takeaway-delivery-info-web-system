function showView(type) {
    if (type === "sender") {
        document.getElementById("blockCount").remove();
        document.getElementById("groupCount").remove();
        document.getElementById("adminCount").remove();
        document.getElementById("senderCount").remove();
        document.getElementById("addBlockTag").remove();
        document.getElementById("addGroupTag").remove();
        document.getElementById("addAdminTag").remove();
        document.getElementById("addSenderTag").remove();
    } else if (type === "admin") {
        document.getElementById("adminCount").remove();
        document.getElementById("blockCount").remove();
        document.getElementById("addBlockTag").remove();
        document.getElementById("addAdminTag").remove();
    }
}

onlyViewOne("addTakeoutView");

window.onpageshow = function () {

    //      doGet   responseText
    //      [{type: , blockCount: , groupCount: , adminCount: , senderCount: , takeoutCount: }]


    let xmlhttp;
    if (window.XMLHttpRequest) {
        //  IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
        xmlhttp = new XMLHttpRequest();
    } else {
        // IE6, IE5 浏览器执行代码
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === XMLHttpRequest.DONE && xmlhttp.status === 200) {
            let data = JSON.parse(xmlhttp.responseText)[0];

            let countEle = ["blockCount", "groupCount", "adminCount", "senderCount", "takeoutCount"];

            for (let id of countEle) {
                document.getElementById(id).firstElementChild.innerHTML = data[id];
            }

            showView(data["type"]);
            changeTakeoutView();
        }
    }
    xmlhttp.open("GET", "more", true);
    // xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xmlhttp.send();
}

function onlyViewOne(id) {
    document.getElementById("moreHint").innerText = "";
    for (const child of document.getElementById("allChangeView").children) {
        if (child.style.display !== "none")
            child.style.display = "none";
    }
    document.getElementById(id).style.display = "";
    for (const ele of document.getElementById(id).getElementsByTagName("select")) {
        for (const option of ele.options) {
            option.remove();
        }
        let idPair = ele.parentElement.id.split("_");
        let optionId = idPair[0] + "Option_" + idPair[1];
        ele.innerHTML = "<option id=\"" + optionId + "\"></option>"
    }
}

function changeTakeoutView() {
    onlyViewOne("addTakeoutView");
    let xmlhttp;
    if (window.XMLHttpRequest) {
        //  IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
        xmlhttp = new XMLHttpRequest();
    } else {
        // IE6, IE5 浏览器执行代码
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === XMLHttpRequest.DONE && xmlhttp.status === 200) {
            let info = JSON.parse(xmlhttp.responseText);

            //  info[0] => name: , type: , user:
            //  info[1]: sender {block, group} , admin {block, groups:[]} , root {blocks:[]}

            let type = info[0]["type"];

            if (type === "sender") {
                document.getElementById("selectBlockOption_addT").outerHTML = "<option value=\"" + info[1]["block"] + "\">" + info[1]["block"] + "</option>";
                document.getElementById("selectGroupOption_addT").outerHTML = "<option value=\"" + info[1]["group"] + "\">" + info[1]["group"] + "</option>";
            } else if (type === "admin") {
                document.getElementById("selectBlockOption_addT").outerHTML = "<option value=\"" + info[1]["block"] + "\">" + info[1]["block"] + "</option>";
                let str = "";
                for (let group of info[1]["groups"]) {
                    str += "<option value=\"" + group + "\">" + group + "</option>";
                }
                document.getElementById("selectGroupOption_addT").outerHTML = str;
            } else if (type === "root") {
                let str = "";
                for (let block of info[1]["blocks"]) {
                    str += "<option value=\"" + block + "\">" + block + "</option>";
                }
                document.getElementById("selectBlockOption_addT").outerHTML = str;
                changeGroupSelect("selectBlock_addT", "selectGroup_addT", "selectGroupOption_addT");
            }


        }
    }
    xmlhttp.open("GET", "change", true);
    xmlhttp.send();
}

function changeSenderView() {
    onlyViewOne("addSenderView");

    let xmlhttp;
    if (window.XMLHttpRequest) {
        //  IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
        xmlhttp = new XMLHttpRequest();
    } else {
        // IE6, IE5 浏览器执行代码
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === XMLHttpRequest.DONE && xmlhttp.status === 200) {
            let info = JSON.parse(xmlhttp.responseText);

            //  info[0] => name: , type: , user:
            //  info[1]: sender {block, group} , admin {block, groups:[]} , root {blocks:[]}

            let type = info[0]["type"];

            if (type === "admin") {
                document.getElementById("selectBlockOption_addS").outerHTML = "<option value=\"" + info[1]["block"] + "\">" + info[1]["block"] + "</option>";
                let str = "";
                for (let group of info[1]["groups"]) {
                    str += "<option value=\"" + group + "\">" + group + "</option>";
                }
                document.getElementById("selectGroupOption_addS").outerHTML = str;
            } else if (type === "root") {
                let str = "";
                for (let block of info[1]["blocks"]) {
                    str += "<option value=\"" + block + "\">" + block + "</option>";
                }
                document.getElementById("selectBlockOption_addS").outerHTML = str;
                changeGroupSelect("selectBlock_addS", "selectGroup_addS", "selectGroupOption_addS");
            }


        }
    }
    xmlhttp.open("GET", "change", true);
    xmlhttp.send();

}

function changeAdminView() {
    onlyViewOne("addAdminView");
    let xmlhttp;
    if (window.XMLHttpRequest) {
        //  IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
        xmlhttp = new XMLHttpRequest();
    } else {
        // IE6, IE5 浏览器执行代码
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === XMLHttpRequest.DONE && xmlhttp.status === 200) {
            let info = JSON.parse(xmlhttp.responseText);

            //  info[0] => name: , type: , user:
            //  info[1]: sender {block, group} , admin {block, groups:[]} , root {blocks:[]}

            let type = info[0]["type"];

            if (type === "root") {
                let str = "";
                for (let block of info[1]["blocks"]) {
                    str += "<option value=\"" + block + "\">" + block + "</option>";
                }
                document.getElementById("selectBlockOption_addA").outerHTML = str;
            }


        }
    }
    xmlhttp.open("GET", "change", true);
    xmlhttp.send();
}

function changeGroupView() {
    onlyViewOne("addGroupView");
    let xmlhttp;
    if (window.XMLHttpRequest) {
        //  IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
        xmlhttp = new XMLHttpRequest();
    } else {
        // IE6, IE5 浏览器执行代码
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === XMLHttpRequest.DONE && xmlhttp.status === 200) {
            let info = JSON.parse(xmlhttp.responseText);

            //  info[0] => name: , type: , user:
            //  info[1]: sender {block, group} , admin {block, groups:[]} , root {blocks:[]}

            let type = info[0]["type"];

            if (type === "admin") {
                document.getElementById("selectBlockOption_addG").outerHTML = "<option value=\"" + info[1]["block"] + "\">" + info[1]["block"] + "</option>";
            } else if (type === "root") {
                let str = "";
                for (let block of info[1]["blocks"]) {
                    str += "<option value=\"" + block + "\">" + block + "</option>";
                }
                document.getElementById("selectBlockOption_addG").outerHTML = str;
            }


        }
    }
    xmlhttp.open("GET", "change", true);
    xmlhttp.send();
}

function changeBlockView() {
    onlyViewOne("addBlockView");
}

function addTakeout() {
    let phone = document.getElementById("customerPhone").value;
    if (!(new RegExp("^\\d{11}$")).test(phone)) {
        document.getElementById("moreHint").innerText = "请正确输入手机号码";
        return;
    }
    let name = document.getElementById("customerName").value;
    if (!(new RegExp("^[\u4E00-\u9FA5A-Za-z0-9_]*$")).test(name)) {
        document.getElementById("moreHint").innerText = "收货人姓名不应该包含特殊字符和空格";
        return;
    }

    if (name.length < 0 || name.length > 20) {
        document.getElementById("moreHint").innerText = "收货人姓名长度应在1至20位";
        return;
    }
    let detail = document.getElementById("detail").value;
    if (detail.length > 200) {
        document.getElementById("moreHint").innerText = "备注长度应在1至200位";
        return;
    }

    let xmlhttp;
    if (window.XMLHttpRequest) {
        //  IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
        xmlhttp = new XMLHttpRequest();
    } else {
        // IE6, IE5 浏览器执行代码
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === XMLHttpRequest.DONE && xmlhttp.status === 200) {
            //      1 - √   0 - add 不匹配     2 - 添加过程失败
            let returnState = parseInt(xmlhttp.responseText);

            if (returnState === 1) {
                document.getElementById("moreHint").innerText = "添加外卖配送信息成功，收货号码: " + phone;
                document.getElementById("customerName").value = "";
                document.getElementById("customerPhone").value = "";
                document.getElementById("detail").value = "";
            } else {
                document.getElementById("moreHint").innerText = "出现未知错误: " + returnState;
            }

        }
    }
    xmlhttp.open("POST", "more", true);
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xmlhttp.send("add=takeout&phone=" + phone + "&name=" + name + "&detail=" + detail + "&block=" +
        getSelected("selectBlock_addT") + "&group=" + getSelected("selectGroup_addT")
    );
}

function addSender() {
    let phone = document.getElementById("senderPhone").value;
    if (!(new RegExp("^\\d{11}$")).test(phone)) {
        document.getElementById("moreHint").innerText = "请正确输入手机号码";
        return;
    }

    let pwd = document.getElementById("senderPassword").value;
    if (!(new RegExp("[a-zA-Z\\d#*+._-]{6,18}")).test(pwd)) {
        document.getElementById("moreHint").innerText = "请正确输入密码(6-18位, 允许特殊符号#*+._-)";
        return;
    }

    let name = document.getElementById("senderName").value;
    if (!(new RegExp("^[\u4E00-\u9FA5A-Za-z0-9_]*$")).test(name)) {
        document.getElementById("moreHint").innerText = "姓名不应该包含特殊字符和空格";
        return;
    }

    if (name.length < 0 || name.length > 20) {
        document.getElementById("moreHint").innerText = "姓名长度应在1至20位";
        return;
    }

    let xmlhttp;
    if (window.XMLHttpRequest) {
        //  IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
        xmlhttp = new XMLHttpRequest();
    } else {
        // IE6, IE5 浏览器执行代码
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === XMLHttpRequest.DONE && xmlhttp.status === 200) {
            //      1 - √   0 - add 不匹配     2 - 添加过程失败      3 - 重复添加
            let returnState = parseInt(xmlhttp.responseText);

            if (returnState === 1) {
                document.getElementById("moreHint").innerText = "添加配送员成功，配送员手机号码: " + phone;
                document.getElementById("senderName").value = "";
                document.getElementById("senderPhone").value = "";
                document.getElementById("senderPassword").value = "";
            } else if (returnState === 3) {
                document.getElementById("moreHint").innerText = "该配送员已存在";
            } else {
                document.getElementById("moreHint").innerText = "出现未知错误: " + returnState;
            }

        }
    }
    xmlhttp.open("POST", "more", true);
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xmlhttp.send("add=sender&phone=" + phone + "&name=" + name + "&password=" + pwd + "&block=" +
        getSelected("selectBlock_addS") + "&group=" + getSelected("selectGroup_addS")
    );
}

function addAdmin() {
    let phone = document.getElementById("adminPhone").value;
    if (!(new RegExp("^\\d{11}$")).test(phone)) {
        document.getElementById("moreHint").innerText = "请正确输入手机号码";
        return;
    }

    let pwd = document.getElementById("adminPassword").value;
    if (!(new RegExp("[a-zA-Z\\d#*+._-]{6,18}")).test(pwd)) {
        document.getElementById("moreHint").innerText = "请正确输入密码(6-18位, 允许特殊符号#*+._-)";
        return;
    }

    let name = document.getElementById("adminName").value;
    if (!(new RegExp("^[\u4E00-\u9FA5A-Za-z0-9_]*$")).test(name)) {
        document.getElementById("moreHint").innerText = "姓名不应该包含特殊字符和空格";
        return;
    }

    if (name.length > 20) {
        document.getElementById("moreHint").innerText = "姓名长度应在1至20位";
        return;
    }

    let xmlhttp;
    if (window.XMLHttpRequest) {
        //  IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
        xmlhttp = new XMLHttpRequest();
    } else {
        // IE6, IE5 浏览器执行代码
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === XMLHttpRequest.DONE && xmlhttp.status === 200) {
            //      1 - √   0 - add 不匹配     2 - 添加过程失败  3 - 重复添加
            let returnState = parseInt(xmlhttp.responseText);

            if (returnState === 1) {
                document.getElementById("moreHint").innerText = "添加管理员成功，管理员手机号码: " + phone;
                document.getElementById("adminName").value = "";
                document.getElementById("adminPhone").value = "";
                document.getElementById("adminPassword").value = "";
            } else if (returnState === 3) {
                document.getElementById("moreHint").innerText = "该管理员已存在";
            } else {
                document.getElementById("moreHint").innerText = "出现未知错误: " + returnState;
            }

        }
    }
    xmlhttp.open("POST", "more", true);
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xmlhttp.send("add=admin&phone=" + phone + "&name=" + name + "&password=" + pwd + "&block=" +
        getSelected("selectBlock_addA")
    );
}

function addGroup() {
    let name = document.getElementById("groupName").value;
    if (!(new RegExp("^[\u4E00-\u9FA5A-Za-z0-9_]+$")).test(name)) {
        document.getElementById("moreHint").innerText = "名称不应该包含特殊字符和空格";
        return;
    }

    if (name.length > 20) {
        document.getElementById("moreHint").innerText = "名称长度应在1至20位";
        return;
    }

    let info = document.getElementById("groupInfo").value;
    if (info.length > 200) {
        document.getElementById("moreHint").innerText = "详细长度应在1至200位";
        return;
    }

    let xmlhttp;
    if (window.XMLHttpRequest) {
        //  IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
        xmlhttp = new XMLHttpRequest();
    } else {
        // IE6, IE5 浏览器执行代码
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === XMLHttpRequest.DONE && xmlhttp.status === 200) {
            //      1 - √   0 - add 不匹配     2 - 添加过程失败  3 - 重复添加
            let returnState = parseInt(xmlhttp.responseText);

            if (returnState === 1) {
                document.getElementById("moreHint").innerText = "添加组别成功，组别: " + name;
                document.getElementById("groupName").value = "";
                document.getElementById("groupInfo").value = "";
            } else if (returnState === 3) {
                document.getElementById("moreHint").innerText = "该组别已存在";
            } else {
                document.getElementById("moreHint").innerText = "出现未知错误: " + returnState;
            }

        }
    }
    xmlhttp.open("POST", "more", true);
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xmlhttp.send("add=group&name=" + name + "&block=" +
        getSelected("selectBlock_addG") + "&info=" + info
    );
}

function addBlock() {
    let name = document.getElementById("blockName").value;
    if (!(new RegExp("^[\u4E00-\u9FA5A-Za-z0-9_]+$")).test(name)) {
        document.getElementById("moreHint").innerText = "名称不应该包含特殊字符和空格";
        return;
    }

    if (name.length > 20) {
        document.getElementById("moreHint").innerText = "名称长度应在1至20位";
        return;
    }

    let info = document.getElementById("blockInfo").value;
    if (info.length > 200) {
        document.getElementById("moreHint").innerText = "详细长度应在1至200位";
        return;
    }

    let xmlhttp;
    if (window.XMLHttpRequest) {
        //  IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
        xmlhttp = new XMLHttpRequest();
    } else {
        // IE6, IE5 浏览器执行代码
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === XMLHttpRequest.DONE && xmlhttp.status === 200) {
            //      1 - √   0 - add 不匹配     2 - 添加过程失败  3 - 重复添加
            let returnState = parseInt(xmlhttp.responseText);

            if (returnState === 1) {
                document.getElementById("moreHint").innerText = "添加区域成功，区域: " + name;
                document.getElementById("blockName").value = "";
                document.getElementById("blockInfo").value = "";
            } else if (returnState === 3) {
                document.getElementById("moreHint").innerText = "该区域已存在";
            } else {
                document.getElementById("moreHint").innerText = "出现未知错误: " + returnState;
            }

        }
    }
    xmlhttp.open("POST", "more", true);
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xmlhttp.send("add=block&name=" + name + "&info=" + info
    );
}



