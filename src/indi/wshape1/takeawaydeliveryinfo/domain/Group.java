package indi.wshape1.takeawaydeliveryinfo.domain;

/**
 * @author Wshape1
 * @create 2022-11-11 12:43
 */

public class Group {
    private String gr_name;
    private String info = null;
    private String block;

    public Group() {
    }

    public Group(String gr_name, String block) {
        this.gr_name = gr_name;
        this.block = block;
    }

    public String getName() {
        return gr_name;
    }

    public void setName(String gr_name) {
        this.gr_name = gr_name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public Sender[] getSenders() {
        return null;
    }

}
