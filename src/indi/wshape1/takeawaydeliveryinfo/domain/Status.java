package indi.wshape1.takeawaydeliveryinfo.domain;

/**
 * @author Wshape1
 * @create 2022-11-07 18:50
 */

public enum Status {
    SENDING("配送中"), SENT("已送达");

    private final String status;

    Status(String s) {
        this.status = s;
    }

    @Override
    public String toString() {
        return this.status;
    }
}
