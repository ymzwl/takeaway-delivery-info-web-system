package indi.wshape1.takeawaydeliveryinfo.service;

import indi.wshape1.takeawaydeliveryinfo.domain.Group;

import java.util.List;

/**
 * @author Wshape1
 * @create 2022-11-12 11:01
 */

public interface GroupService extends RangeService<Group> {

    List<Group> getListByBlock(String block);

    List<Group> getListByBlockByKeywordAndPage(String block, String keyword, int page);

    int getCountByBlock(String block);
}
