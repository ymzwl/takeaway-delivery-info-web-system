DROP USER IF EXISTS 'take_out';
CREATE USER 'take_out'@'%' IDENTIFIED BY 'take_out';
GRANT ALL ON `take_out`.* TO 'take_out'@'%';