package indi.wshape1.takeawaydeliveryinfo.service.impl;

import indi.wshape1.takeawaydeliveryinfo.dao.SenderDAO;
import indi.wshape1.takeawaydeliveryinfo.dao.impl.SenderDAOImpl;
import indi.wshape1.takeawaydeliveryinfo.domain.Group;
import indi.wshape1.takeawaydeliveryinfo.domain.Sender;
import indi.wshape1.takeawaydeliveryinfo.service.BlockService;
import indi.wshape1.takeawaydeliveryinfo.service.GroupService;
import indi.wshape1.takeawaydeliveryinfo.service.SenderService;
import indi.wshape1.takeawaydeliveryinfo.util.StringUtil;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Wshape1
 * @create 2022-11-12 10:16
 */

public class SenderServiceImpl implements SenderService {

    private final SenderDAO senderDAO = new SenderDAOImpl();

    @Override
    public Sender getByPhone(String phone) {
        if (StringUtil.checkPhone(phone)) {
            return senderDAO.getByPhone(phone);
        }
        return null;
    }

    @Override
    public boolean add(Sender sender) {
        BlockService blockService = new BlockServiceImpl();
        GroupService groupService = new GroupServiceImpl();
        Group group = groupService.getByName(sender.getGroup());
        if (group != null) {
            if (blockService.getByName(sender.getBlock()) != null && group.getBlock().equals(sender.getBlock())) {
                if (getByPhone(sender.getPhone()) == null) {
                    senderDAO.add(sender);
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    public boolean delByPhone(String phone) throws SQLException {
        if (getByPhone(phone) != null) {
            senderDAO.delByPhone(phone);
            return true;
        }
        return false;
    }

    @Override
    public boolean update(Sender sender, String oldPhone) throws SQLException {
        BlockService blockService = new BlockServiceImpl();
        GroupService groupService = new GroupServiceImpl();
        Group group = groupService.getByName(sender.getGroup());
        if (group != null) {
            if (blockService.getByName(sender.getBlock()) != null && group.getBlock().equals(sender.getBlock())) {

                if (getByPhone(sender.getPhone()) != null) {
                    if (oldPhone.equals(sender.getPhone()) || getByPhone(sender.getPhone()) == null) {
                        senderDAO.update(sender, oldPhone);
                        return true;
                    }
                }

            }
        }
        return false;
    }

    @Override
    public List<Sender> getList() {
        return senderDAO.getList();
    }

    @Override
    public List<Sender> getListByPage(int page) {
        return senderDAO.getListByPage(page);
    }

    @Override
    public List<Sender> getListByKeywordAndPage(String keyword, int page) {
        return senderDAO.getListByKeywordAndPage(keyword, page);
    }

    @Override
    public List<Sender> getListByKeyword(String keyword) {
        return senderDAO.getListByKeyword(keyword);
    }

    @Override
    public int getCount() {
        return senderDAO.getCount();
    }

    @Override
    public int getCountByKeyword(String keyword) {
        return senderDAO.getCountByKeyword(keyword);
    }

    @Override
    public int getPageCount() {
        return (getCount() - 1 + senderDAO.getCountPerPage()) / senderDAO.getCountPerPage();
    }

    @Override
    public int getPageCountByKeyword(String keyword) {
        return (getCountByKeyword(keyword) - 1 + senderDAO.getCountPerPage()) / senderDAO.getCountPerPage();
    }

    @Override
    public List<Sender> getListByBlockByKeywordAndPage(String block, String keyword, int page) {
        return senderDAO.getListByBlockByKeywordAndPage(block, keyword, page);
    }

    @Override
    public List<Sender> getListByGroup(String group) {
        return senderDAO.getListByGroup(group);
    }

    @Override
    public List<Sender> getListByBlock(String block) {
        return senderDAO.getListByBlock(block);
    }

    @Override
    public List<Sender> getListByGroupAndBlock(String group, String block) {
        return senderDAO.getListByGroupAndBlock(group, block);
    }

    @Override
    public boolean freeSenderByGroup(String group) throws SQLException {
        return senderDAO.batchUpdateBy("group", new String[]{"group", "block"}, "null", "null", group);
    }

    @Override
    public boolean freeSenderByBlock(String block) throws SQLException {
        return senderDAO.batchUpdateBy("block", new String[]{"group", "block"}, "null", "null", block);
    }

    @Override
    public List<Sender> getListByGroupAndBlockByKeywordAndPage(String group, String block, String keyword, int page) {
        return senderDAO.getListByGroupAndBlockByKeywordAndPage(group, block, keyword, page);
    }

    @Override
    public int getCountByBlock(String block) {
        return senderDAO.getCountByBlock(block);
    }

}
