package indi.wshape1.takeawaydeliveryinfo;

import indi.wshape1.takeawaydeliveryinfo.dao.impl.TakeOutDAOImpl;
import indi.wshape1.takeawaydeliveryinfo.domain.TakeOut;
import indi.wshape1.takeawaydeliveryinfo.util.ConnectionUtil;
import org.testng.annotations.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Wshape1
 * @create 2022-11-10 18:32
 */

public class test {

    public static void main(String[] args) {

        try {
            Connection connection = ConnectionUtil.getDataSource().getConnection();

            PreparedStatement preparedStatement = connection.prepareStatement("call getTakeOutListByGroupAndBlockByKeywordAndPage('group1', 'area1', '%', 1, 5)");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                String phone = resultSet.getString("phone");
                System.out.println(phone);
            }


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }


    @Test
    public void test() {

        TakeOutDAOImpl takeOutDAO = new TakeOutDAOImpl();
        for (TakeOut takeOut : takeOutDAO.getListByGroupAndBlockByKeywordAndPage("group1", "area1", "", 1)) {
            System.out.println(takeOut);
        }


    }

    @Test
    public void test1() throws SQLException {

        try {
            abc();
            System.out.println("没事");
        } catch (Exception e) {
            System.out.println("咋抓");
        }


    }

    private void abc() {
        try {
            Integer.parseInt("adsasd");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


}
