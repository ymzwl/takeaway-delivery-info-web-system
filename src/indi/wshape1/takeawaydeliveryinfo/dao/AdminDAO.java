package indi.wshape1.takeawaydeliveryinfo.dao;

import indi.wshape1.takeawaydeliveryinfo.domain.Admin;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Wshape1
 * @create 2022-11-10 18:51
 */

public interface AdminDAO extends StaffDAO<Admin> {

    int countPerPage = 100;

    default int getCountPerPage() {
        return countPerPage;
    }

    List<Admin> getListByBlock(String block);

    boolean batchUpdateByBlock(String[] attrs, String... valAndLastLimitBlock) throws SQLException;

}
