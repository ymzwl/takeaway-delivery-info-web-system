function getSelected(optionId) {
    let options = document.getElementById(optionId).getElementsByTagName("select").item(0).options;
    for (let i = 0; i < options.length; i++) {
        if (options.item(i).selected)
            return options.item(i).value;
    }
}

function changeGroupSelect(id, beChangeId, beChangeOptionId) {

    let selectBlock = getSelected(id);
    if (selectBlock === "-") {
        document.getElementById(beChangeId).firstElementChild.innerHTML = "<option value=\"-\"></option>" +
            "<option id=\"" + beChangeOptionId + "\" style=\"display: none\"></option>";
        return;
    }

    let xmlhttp;
    if (window.XMLHttpRequest) {
        //  IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
        xmlhttp = new XMLHttpRequest();
    } else {
        // IE6, IE5 浏览器执行代码
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === XMLHttpRequest.DONE && xmlhttp.status === 200) {

            let list = JSON.parse(xmlhttp.responseText);
            let path = window.location.pathname.split("/");
            let page = path[path.length - 1];
            document.getElementById(beChangeId).getElementsByTagName("select").item(0).innerHTML =
                (page === "more.html" ? "" : "<option value=\"-\"></option>") +
                "<option id=\"" + beChangeOptionId + "\" style=\"display: none\"></option>";

            let str = "";

            for (let group of list) {
                str += "<option value=\"" + group + "\">" + group + "</option>";
            }

            document.getElementById(beChangeOptionId).outerHTML = str;

        }
    }
    xmlhttp.open("GET", "change?selectedBlock=" + selectBlock, true);
    xmlhttp.send();
}