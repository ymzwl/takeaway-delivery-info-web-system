package indi.wshape1.takeawaydeliveryinfo.util;

import indi.wshape1.takeawaydeliveryinfo.domain.Status;

/**
 * @author Wshape1
 * @create 2022-11-11 12:55
 */

public class StringUtil {

    public static boolean isEmpty(String str) {
        return str == null || "".equals(str);
    }

    public static boolean isNumAll(String num) {
        try {
            Long.parseLong(num);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean checkPhone(String phone) {
        return !isEmpty(phone) && phone.length() <= 11 && isNumAll(phone);
    }

    public static boolean isSendingStatus(String status) {
        return status.equals(Status.SENDING.toString());
    }

}
