package indi.wshape1.takeawaydeliveryinfo.service.impl;

import indi.wshape1.takeawaydeliveryinfo.dao.GroupDAO;
import indi.wshape1.takeawaydeliveryinfo.dao.impl.GroupDAOImpl;
import indi.wshape1.takeawaydeliveryinfo.domain.Group;
import indi.wshape1.takeawaydeliveryinfo.service.BlockService;
import indi.wshape1.takeawaydeliveryinfo.service.GroupService;
import indi.wshape1.takeawaydeliveryinfo.service.SenderService;
import indi.wshape1.takeawaydeliveryinfo.service.TakeOutService;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Wshape1
 * @create 2022-11-12 11:02
 */

public class GroupServiceImpl implements GroupService {
    private final GroupDAO groupDAO = new GroupDAOImpl();


    @Override
    public Group getByName(String name) {
        return groupDAO.getByName(name);
    }

    @Override
    public boolean add(Group group) {
        BlockService blockService = new BlockServiceImpl();
        if (getByName(group.getName()) == null && blockService.getByName(group.getBlock()) != null) {
            groupDAO.add(group);
            return true;
        }
        return false;
    }

    @Override
    public boolean delByName(String name) throws SQLException {
        // do more
        Group group = getByName(name);
        if (!"null".equals(name) && group != null) {
            // del takeout_info s
            TakeOutService takeOutService = new TakeOutServiceImpl();
            takeOutService.deleteByBlockGroup(group.getBlock(), name);
            SenderService senderService = new SenderServiceImpl();
            senderService.freeSenderByGroup(name);
            groupDAO.delByName(name);
            return true;
        }
        return false;
    }

    @Override
    public boolean update(Group group, String oldName) throws SQLException {
        BlockService blockService = new BlockServiceImpl();

        if (getByName(group.getName()) != null && blockService.getByName(group.getBlock()) != null) {
            if (oldName.equals(group.getName()) || getByName(group.getName()) == null) {
                groupDAO.update(group, oldName);
                return true;
            }
        }
        return false;
    }

    @Override
    public List<Group> getList() {
        return groupDAO.getList();
    }

    @Override
    public List<Group> getListByPage(int page) {
        return groupDAO.getListByPage(page);
    }

    @Override
    public List<Group> getListByKeywordAndPage(String keyword, int page) {
        return groupDAO.getListByKeywordAndPage(keyword, page);
    }

    @Override
    public List<Group> getListByKeyword(String keyword) {
        return groupDAO.getListByKeyword(keyword);
    }

    @Override
    public int getCount() {
        return groupDAO.getCount();
    }

    @Override
    public int getCountByKeyword(String keyword) {
        return groupDAO.getCountByKeyword(keyword);
    }

    @Override
    public int getPageCount() {
        return (getCount() - 1 + groupDAO.getCountPerPage()) / groupDAO.getCountPerPage();
    }

    @Override
    public int getPageCountByKeyword(String keyword) {
        return (getCountByKeyword(keyword) - 1 + groupDAO.getCountPerPage()) / groupDAO.getCountPerPage();
    }

    @Override
    public List<Group> getListByBlock(String block) {
        return groupDAO.getListByBlock(block);
    }

    @Override
    public List<Group> getListByBlockByKeywordAndPage(String block, String keyword, int page) {
        return groupDAO.getListByBlockByKeywordAndPage(block, keyword, page);
    }

    @Override
    public int getCountByBlock(String block) {
        return groupDAO.getCountByBlock(block);
    }
}
