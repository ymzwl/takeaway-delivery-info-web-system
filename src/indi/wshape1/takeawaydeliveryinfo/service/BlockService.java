package indi.wshape1.takeawaydeliveryinfo.service;

import indi.wshape1.takeawaydeliveryinfo.domain.Block;

/**
 * @author Wshape1
 * @create 2022-11-12 10:31
 */

public interface BlockService extends RangeService<Block> {

}
