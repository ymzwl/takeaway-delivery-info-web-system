package indi.wshape1.takeawaydeliveryinfo.dao.impl;

import indi.wshape1.takeawaydeliveryinfo.dao.AdminDAO;
import indi.wshape1.takeawaydeliveryinfo.domain.Admin;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Wshape1
 * @create 2022-11-10 18:52
 */

public class AdminDAOImpl extends BaseDAOImpl<Admin> implements AdminDAO {

    @Override
    public Admin getByPhone(String phone) {
        List<Admin> admins = executeQuery("SELECT `ad_name`, `phone`, `password`, `block` FROM `admin` WHERE `phone` = ? LIMIT 1", phone);
        if (admins.size() > 0)
            return admins.get(0);
        return null;
    }

    @Override
    public void add(Admin admin) {
        try {
            executeUpdate("INSERT INTO `admin`(`ad_name`, `phone`, `password`, `block`) VALUES (?, ?, ?, ?)",
                    admin.getName(),
                    admin.getPhone(),
                    admin.getPassword(),
                    admin.getBlock()
            );
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delByPhone(String phone) throws SQLException {
        executeUpdate("DELETE FROM `admin` WHERE `phone` = ? LIMIT 1", phone);
    }

    @Override
    public void update(Admin admin, String oldPhone) throws SQLException {
        executeUpdate("UPDATE `admin` SET `ad_name` = ?, `phone` = ?, `password` = ?, `block` = ? WHERE `phone` = ? LIMIT 1",
                admin.getName(),
                admin.getPhone(),
                admin.getPassword(),
                admin.getBlock(),
                oldPhone
        );
    }

    @Override
    public List<Admin> getList() {
        return executeQuery("SELECT `ad_name`, `phone`, `password`, `block` FROM `admin`");
    }

    @Override
    public List<Admin> getListByPage(int page) {
        return executeQuery("SELECT `ad_name`, `phone`, `password`, `block` FROM `admin` LIMIT ?, ?",
                (page - 1) * countPerPage,
                countPerPage);
    }

    @Override
    public List<Admin> getListByKeywordAndPage(String keyword, int page) {
        return executeQuery("SELECT `ad_name`, `phone`, `password`, `block` FROM `admin` WHERE `ad_name` LIKE ? or `phone` LIKE ? or `block` LIKE ? LIMIT ?, ?",
                "%" + keyword + "%",
                "%" + keyword + "%",
                "%" + keyword + "%",
                (page - 1) * countPerPage,
                countPerPage);
    }

    @Override
    public List<Admin> getListByKeyword(String keyword) {
        return executeQuery("SELECT `ad_name`, `phone`, `password`, `block` FROM `admin` WHERE `ad_name` LIKE ? or `phone` LIKE ? or `block` LIKE ?",
                "%" + keyword + "%",
                "%" + keyword + "%",
                "%" + keyword + "%");
    }

    @Override
    public int getCount() {
        return ((Long) executeComplexQuery("SELECT COUNT(*) FROM `admin`")[0]).intValue();
    }

    @Override
    public int getCountByKeyword(String keyword) {
        return ((Long) executeComplexQuery(
                "SELECT COUNT(*) FROM `admin` WHERE `ad_name` LIKE ? or `phone` LIKE ? or `block` LIKE ?",
                "%" + keyword + "%",
                "%" + keyword + "%",
                "%" + keyword + "%"
        )[0]).intValue();
    }

    @Override
    public List<Admin> getListByBlockByKeywordAndPage(String block, String keyword, int page) {
        return executeQuery("call getAdminListByBlockByKeywordAndPage('" + block + "', '%" +
                keyword +
                "%', " + (page - 1) * countPerPage + ", " + countPerPage + ")"
        );
    }

    @Override
    public List<Admin> getListByBlock(String block) {
        return executeQuery("SELECT `ad_name`, `phone`, `password`, `block` FROM `admin` WHERE `block` = ?", block);
    }

    @Override
    public boolean batchUpdateByBlock(String[] attrs, String... valAndLastLimitBlock) throws SQLException {
        if (attrs.length + 1 == valAndLastLimitBlock.length && attrs.length > 0) {
            StringBuilder stringBuffer = new StringBuilder("UPDATE `admin` SET ");
            stringBuffer.append(attrs[0]);
            stringBuffer.append(" = ?");
            for (int i = 1; i < attrs.length; i++) {
                stringBuffer.append(", `");
                stringBuffer.append(attrs[i]);
                stringBuffer.append("` = ?");
            }
            stringBuffer.append(" WHERE `block` = ?");
            executeUpdate(stringBuffer.toString(), valAndLastLimitBlock);
            return true;
        }
        return false;
    }
}
