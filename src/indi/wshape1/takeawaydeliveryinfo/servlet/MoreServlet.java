package indi.wshape1.takeawaydeliveryinfo.servlet;

import indi.wshape1.takeawaydeliveryinfo.domain.*;
import indi.wshape1.takeawaydeliveryinfo.service.UtilService;
import indi.wshape1.takeawaydeliveryinfo.util.StringUtil;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Wshape1
 * @create 2022-11-26 20:30
 */

@WebServlet(name = "MoreServlet", value = {"/more"})
public class MoreServlet extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html; charset=UTF-8");
        PrintWriter writer = resp.getWriter();

        String user = (String) req.getSession().getAttribute("user");
        String type = (String) req.getServletContext().getAttribute(user);

        StringBuilder stringBuilder = new StringBuilder("[{");


        int blockCount = 0;
        int groupCount = 0;
        int adminCount = 0;
        int senderCount = 0;
        int takeoutCount;

        if ("sender".equals(type)) {
            Sender sender = UtilService.senderService.getByPhone(user);
            takeoutCount = UtilService.takeOutService.getCountByBlockAndGroup(sender.getBlock(), sender.getGroup());
        } else if ("admin".equals(type)) {
            String block = UtilService.adminService.getByPhone(user).getBlock();
            takeoutCount = UtilService.takeOutService.getCountByBlock(block);
            senderCount = UtilService.senderService.getCountByBlock(block);
            groupCount = UtilService.groupService.getCountByBlock(block);
        } else {
            takeoutCount = UtilService.takeOutService.getCount();
            senderCount = UtilService.senderService.getCount();
            groupCount = UtilService.groupService.getCount();
            adminCount = UtilService.adminService.getCount();
            blockCount = UtilService.blockService.getCount();
        }
        stringBuilder.append("\"type\": \"").append(type).append("\",")
                .append("\"blockCount\": \"").append(blockCount).append("\",")
                .append("\"groupCount\": \"").append(groupCount).append("\",")
                .append("\"adminCount\": \"").append(adminCount).append("\",")
                .append("\"senderCount\": \"").append(senderCount).append("\",")
                .append("\"takeoutCount\": \"").append(takeoutCount).append("\"}]");

        writer.print(stringBuilder);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html; charset=UTF-8");

        try {

            PrintWriter writer = resp.getWriter();

            //      1 - √   0 - add 不匹配     2 - 添加过程失败 3 - 实体已存在    4 - 没有权限
            int returnState = 0;
            String add = req.getParameter("add");
            String user = (String) req.getSession().getAttribute("user");
            String type = (String) req.getSession().getAttribute(user);
            String selectBlock = req.getParameter("block");
            String selectGroup = req.getParameter("group");

            if (!"root".equals(type) && ("null".equals(selectBlock) || "null".equals(selectGroup))) {
                writer.print(4);
                return;
            }


            switch (add) {
                case "takeout" -> {
                    String name = req.getParameter("name");
                    String detail = req.getParameter("detail");
                    String phone = req.getParameter("phone");
                    TakeOut takeOut = new TakeOut(selectBlock, selectGroup, phone, user);
                    if (!StringUtil.isEmpty(name))
                        takeOut.setName(name);
                    if (!StringUtil.isEmpty(detail))
                        takeOut.setDetail(detail);
                    if (UtilService.takeOutService.addInfo(takeOut))
                        returnState = 1;
                    else
                        returnState = 2;
                }
                case "sender" -> {
                    String name = req.getParameter("name");
                    String phone = req.getParameter("phone");
                    String password = req.getParameter("password");
                    if (UtilService.senderService.getByPhone(phone) != null) {
                        returnState = 3;
                        break;
                    }
                    Sender sender = new Sender(phone, password, selectBlock, selectGroup);
                    if (!StringUtil.isEmpty(name))
                        sender.setName(name);
                    if (UtilService.senderService.add(sender))
                        returnState = 1;
                    else
                        returnState = 2;
                }
                case "admin" -> {
                    String name = req.getParameter("name");
                    String phone = req.getParameter("phone");
                    String password = req.getParameter("password");

                    if (UtilService.adminService.getByPhone(phone) != null) {
                        returnState = 3;
                        break;
                    }
                    Admin admin = new Admin(phone, password, selectBlock);
                    if (!StringUtil.isEmpty(name))
                        admin.setName(name);
                    if (UtilService.adminService.add(admin))
                        returnState = 1;
                    else
                        returnState = 2;
                }
                case "group" -> {
                    String name = req.getParameter("name");
                    String info = req.getParameter("info");

                    if (UtilService.groupService.getByName(name) != null) {
                        returnState = 3;
                        break;
                    }
                    Group group = new Group(name, selectBlock);
                    if (!StringUtil.isEmpty(info))
                        group.setInfo(info);
                    if (UtilService.groupService.add(group))
                        returnState = 1;
                    else
                        returnState = 2;
                }
                case "block" -> {
                    String name = req.getParameter("name");
                    String info = req.getParameter("info");

                    if (UtilService.blockService.getByName(name) != null) {
                        returnState = 3;
                        break;
                    }
                    Block block = new Block(name);
                    if (!StringUtil.isEmpty(name))
                        block.setInfo(name);
                    if (UtilService.blockService.add(block))
                        returnState = 1;
                    else
                        returnState = 2;
                }
                default -> System.out.println(add);
            }

            writer.print(returnState);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
