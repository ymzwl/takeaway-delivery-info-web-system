package indi.wshape1.takeawaydeliveryinfo.dao.impl;

import indi.wshape1.takeawaydeliveryinfo.dao.GroupDAO;
import indi.wshape1.takeawaydeliveryinfo.domain.Group;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Wshape1
 * @create 2022-11-11 21:18
 */

public class GroupDAOImpl extends BaseDAOImpl<Group> implements GroupDAO {
    @Override
    public Group getByName(String name) {
        List<Group> groups = executeQuery("SELECT `gr_name`, `info`, `block` FROM `group` WHERE `gr_name` = ? LIMIT 1", name);
        if (groups.size() > 0)
            return groups.get(0);
        return null;
    }

    @Override
    public void add(Group group) {
        try {
            executeUpdate("INSERT INTO `group`(`gr_name`, `info`, `block`) VALUES (?, ?, ?)", group.getName(), group.getInfo(), group.getBlock());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delByName(String name) throws SQLException {
        executeUpdate("DELETE FROM `group` WHERE `gr_name` = ?", name);
    }

    @Override
    public void update(Group group, String oldName) throws SQLException {
        executeUpdate("UPDATE `group` SET `gr_name` = ?, `info` = ?, `block` = ? WHERE `gr_name` = ?",
                group.getName(),
                group.getInfo(),
                group.getBlock(),
                oldName
        );
    }

    @Override
    public List<Group> getList() {
        return executeQuery("SELECT `gr_name`, `info` FROM `group`");
    }

    @Override
    public List<Group> getListByPage(int page) {
        return executeQuery("SELECT `gr_name`, `info`, `block` FROM `group` LIMIT ?, ?",
                (page - 1) * countPerPage,
                countPerPage
        );
    }

    @Override
    public List<Group> getListByKeywordAndPage(String keyword, int page) {
        return executeQuery("SELECT `gr_name`, `info`, `block` FROM `group` WHERE `gr_name` LIKE ? or `info` LIKE ? or `block` LIKE ? LIMIT ?, ?",
                "%" + keyword + "%",
                "%" + keyword + "%",
                "%" + keyword + "%",
                (page - 1) * countPerPage,
                countPerPage
        );
    }

    @Override
    public List<Group> getListByKeyword(String keyword) {
        return executeQuery("SELECT `gr_name`, `info`, `block` FROM `group` WHERE `gr_name` LIKE ? or `info` LIKE ? or `block` LIKE ?",
                "%" + keyword + "%",
                "%" + keyword + "%",
                "%" + keyword + "%"
        );
    }

    @Override
    public int getCount() {
        return ((Long) executeComplexQuery("SELECT COUNT(*) FROM `group`")[0]).intValue();
    }

    @Override
    public int getCountByKeyword(String keyword) {
        return ((Long) executeComplexQuery(
                "SELECT COUNT(*) FROM `group` WHERE `gr_name` LIKE ? or `info` LIKE ? or `block` LIKE ?",
                "%" + keyword + "%",
                "%" + keyword + "%",
                "%" + keyword + "%"
        )[0]).intValue();
    }

    @Override
    public List<Group> getListByBlock(String block) {
        return executeQuery("SELECT `gr_name`, `info`, `block` FROM `group` WHERE `block` = ?", block);
    }

    @Override
    public List<Group> getListByBlockByKeywordAndPage(String block, String keyword, int page) {
        return executeQuery("SELECT `gr_name`, `info`, `block` FROM `group` WHERE `block` = ? AND (`gr_name` LIKE ? or `info` LIKE ? or `block` LIKE ?) LIMIT ?, ?",
                block,
                "%" + keyword + "%",
                "%" + keyword + "%",
                "%" + keyword + "%",
                (page - 1) * countPerPage,
                countPerPage
        );
    }

    @Override
    public int getCountByBlock(String block) {
        return ((Long) executeComplexQuery(
                "SELECT COUNT(*) FROM `group` WHERE `block` = ?", block
        )[0]).intValue();
    }
}
