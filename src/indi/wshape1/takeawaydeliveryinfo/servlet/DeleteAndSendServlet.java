package indi.wshape1.takeawaydeliveryinfo.servlet;

import indi.wshape1.takeawaydeliveryinfo.domain.Status;
import indi.wshape1.takeawaydeliveryinfo.domain.TakeOut;
import indi.wshape1.takeawaydeliveryinfo.service.UtilService;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * @author Wshape1
 * @create 2022-11-28 21:04
 */

@WebServlet(name = "DeleteAndSendServlet", value = {"/delete", "/send"})
public class DeleteAndSendServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/plain; charset=UTF-8");

        try {
            BufferedReader reader = req.getReader();
            PrintWriter writer = resp.getWriter();

            String servletPath = req.getServletPath().substring(1);

            String[] ids = reader.readLine().split(":");
            String selectType = ids[ids.length - 1];

            StringBuilder stringBuilder = new StringBuilder("[{\"state\": \"");

            ArrayList<String> fail = new ArrayList<>();
            int state = 0;


            if ("send".equals(servletPath)) {
                if ("takeout".equals(selectType)) {
                    for (int i = 1; i < ids.length - 1; i++) {
                        TakeOut takeOut = UtilService.takeOutService.getById(Integer.parseInt(ids[i]));
                        if (takeOut != null) {
                            if (takeOut.getStatus() == Status.SENDING) {

                                //  送达操作提醒  如：短信提醒
                                // do more

                                takeOut.setStatus(Status.SENT);
                            } else {
                                takeOut.setStatus(Status.SENDING);
                            }
                            if (!UtilService.takeOutService.updateInfo(takeOut)) {
                                fail.add(ids[i]);
                            }
                        } else {
                            fail.add(ids[i]);
                        }
                    }
                    state = 1;
                }
            } else {       // "/delete"         sender admin: phone     group block: name
                switch (selectType) {
                    case "takeout" -> {
                        for (int i = 1; i < ids.length - 1; i++) {
                            if (!UtilService.takeOutService.deleteInfo(Integer.parseInt(ids[i])))
                                fail.add(ids[i]);
                        }
                        state = 1;
                    }
                    case "sender" -> {
                        for (int i = 1; i < ids.length - 1; i++) {
                            if (req.getServletContext().getAttribute(ids[i]) != null || !UtilService.senderService.delByPhone(ids[i]))
                                fail.add(ids[i]);
                        }
                        state = 1;
                    }
                    case "admin" -> {
                        for (int i = 1; i < ids.length - 1; i++) {
                            if (req.getServletContext().getAttribute(ids[i]) != null || !UtilService.adminService.delByPhone(ids[i]))
                                fail.add(ids[i]);
                        }
                        state = 1;
                    }

                    case "group" -> {
                        for (int i = 1; i < ids.length - 1; i++) {
                            if (!UtilService.groupService.delByName(ids[i]))
                                fail.add(ids[i]);
                        }
                        state = 1;
                    }

                    case "block" -> {
                        for (int i = 1; i < ids.length - 1; i++) {
                            if (!UtilService.blockService.delByName(ids[i]))
                                fail.add(ids[i]);
                        }
                        state = 1;
                    }
                }

            }

            if (fail.size() == ids.length)
                state = 0;
            else if (fail.size() > 0)
                state = 2;

            stringBuilder.append(state).append("\"}, {\"fail\": [");
            for (String f : fail) {
                stringBuilder.append("\"").append(f).append("\",");
            }
            if (fail.size() > 0)
                stringBuilder.deleteCharAt(stringBuilder.length() - 1);
            stringBuilder.append("]}]");
            writer.print(stringBuilder);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
