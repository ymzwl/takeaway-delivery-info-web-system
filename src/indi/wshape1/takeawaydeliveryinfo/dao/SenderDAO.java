package indi.wshape1.takeawaydeliveryinfo.dao;

import indi.wshape1.takeawaydeliveryinfo.domain.Sender;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Wshape1
 * @create 2022-11-11 11:42
 */

public interface SenderDAO extends StaffDAO<Sender> {
    int countPerPage = 100;

    default int getCountPerPage() {
        return countPerPage;
    }

    List<Sender> getListByGroup(String group);

    List<Sender> getListByBlock(String block);

    List<Sender> getListByGroupAndBlock(String group, String block);

    boolean batchUpdateBy(String limitAttr, String[] attrs, String... valAndLastLimitVal) throws SQLException;

    List<Sender> getListByGroupAndBlockByKeywordAndPage(String group, String block, String keyword, int page);

    int getCountByBlock(String block);
}
