package indi.wshape1.takeawaydeliveryinfo.service.impl;

import indi.wshape1.takeawaydeliveryinfo.dao.TakeOutDAO;
import indi.wshape1.takeawaydeliveryinfo.dao.impl.TakeOutDAOImpl;
import indi.wshape1.takeawaydeliveryinfo.domain.Group;
import indi.wshape1.takeawaydeliveryinfo.domain.TakeOut;
import indi.wshape1.takeawaydeliveryinfo.service.BlockService;
import indi.wshape1.takeawaydeliveryinfo.service.GroupService;
import indi.wshape1.takeawaydeliveryinfo.service.TakeOutService;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Wshape1
 * @create 2022-11-16 19:06
 */

public class TakeOutServiceImpl implements TakeOutService {

    TakeOutDAO takeOutDAO = new TakeOutDAOImpl();

    @Override
    public List<TakeOut> getListByGroupAndBlockByPage(String group, String block, int page) {
        return takeOutDAO.getListByGroupAndBlockByPage(group, block, page);
    }

    @Override
    public List<TakeOut> getListByGroupAndBlockByKeywordAndPage(String group, String block, String keyword, int page) {
        return takeOutDAO.getListByGroupAndBlockByKeywordAndPage(group, block, keyword, page);
    }

    @Override
    public int getPageCountByGroupAndBlock(String group, String block, String keyword) {
        return takeOutDAO.getPageCountByGroupAndBlock(group, block, keyword);
    }

    @Override
    public List<TakeOut> getListByBlockByPage(String block, int page) {
        return takeOutDAO.getListByBlockByPage(block, page);
    }

    @Override
    public List<TakeOut> getListByBlockByKeywordAndPage(String block, String keyword, int page) {
        return takeOutDAO.getListByBlockByKeywordAndPage(block, keyword, page);
    }

    @Override
    public int getPageCountByBlock(String block, String keyword) {
        return takeOutDAO.getPageCountByBlock(block, keyword);
    }

    @Override
    public List<TakeOut> getListBySenderPhoneByPage(String se_phone, int page) {
        return takeOutDAO.getListBySenderPhoneByPage(se_phone, page);
    }

    @Override
    public List<TakeOut> getListBySenderPhoneByKeywordAndPage(String se_phone, String keyword, int page) {
        return takeOutDAO.getListBySenderPhoneByKeywordAndPage(se_phone, keyword, page);
    }

    @Override
    public int getPageCountBySenderPhone(String se_phone, String keyword) {
        return takeOutDAO.getPageCountBySenderPhone(se_phone, keyword);
    }

    @Override
    public List<TakeOut> getListByPhonePage(String phone, int page) {
        return takeOutDAO.getListByPhonePage(phone, page);
    }

    @Override
    public int getPageCountByPhone(String phone) {
        return takeOutDAO.getPageCountByPhone(phone);
    }

    @Override
    public boolean updateInfo(TakeOut takeOut) throws SQLException {
        if (getById(takeOut.getId()) != null) {
            BlockService blockService = new BlockServiceImpl();
            GroupService groupService = new GroupServiceImpl();
            Group group = groupService.getByName(takeOut.getGroup());
            if (group != null) {
                if (blockService.getByName(takeOut.getBlock()) != null && group.getBlock().equals(takeOut.getBlock())) {
                    takeOutDAO.updateInfo(takeOut);
                    return true;
                }

            }
        }
        return false;
    }

    @Override
    public boolean deleteInfo(int id) throws SQLException {
        if (id < 0)
            return false;
        takeOutDAO.deleteInfo(id);
        return true;
    }

    @Override
    public boolean addInfo(TakeOut takeOut) {
        if (takeOut.getId() != null && getById(takeOut.getId()) != null)
            return false;
        BlockService blockService = new BlockServiceImpl();
        GroupService groupService = new GroupServiceImpl();
        Group group = groupService.getByName(takeOut.getGroup());
        if (group != null) {
            if (blockService.getByName(takeOut.getBlock()) != null && group.getBlock().equals(takeOut.getBlock())) {
                takeOutDAO.addInfo(takeOut);
                return true;
            }

        }
        return false;
    }

    @Override
    public TakeOut getById(int id) {
        if (id < 0)
            return null;
        return takeOutDAO.getById(id);
    }

    @Override
    public boolean deleteByBlockGroup(String block, String group) throws SQLException {
        BlockService blockService = new BlockServiceImpl();
        GroupService groupService = new GroupServiceImpl();
        Group group1 = groupService.getByName(group);
        if (group1 != null) {
            if (blockService.getByName(block) != null && block.equals(group1.getBlock())) {
                takeOutDAO.deleteByBlockGroup(block, group);
                return true;
            }

        }
        return false;
    }

    @Override
    public boolean deleteByBlock(String block) throws SQLException {
        BlockService blockService = new BlockServiceImpl();
        if (blockService.getByName(block) != null) {
            takeOutDAO.deleteByBlock(block);
            return true;
        }
        return false;
    }

    @Override
    public List<TakeOut> getListBetweenByPage(String start, String end, int page) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {
            simpleDateFormat.parse(start);
            simpleDateFormat.parse(end);
        } catch (ParseException e) {
            return new ArrayList<>();
        }

        return takeOutDAO.getListBetweenByPage(start, end, page);
    }

    @Override
    public List<TakeOut> getListBetweenByKeywordAndPage(String start, String end, String keyword, int page) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {
            simpleDateFormat.parse(start);
            simpleDateFormat.parse(end);
        } catch (ParseException e) {
            return new ArrayList<>();
        }

        return takeOutDAO.getListBetweenByKeywordAndPage(start, end, keyword, page);
    }

    @Override
    public int getPageCountBetween(String start, String end, String keyword) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {
            simpleDateFormat.parse(start);
            simpleDateFormat.parse(end);
        } catch (ParseException e) {
            return 0;
        }
        return takeOutDAO.getPageCountBetween(start, end, keyword);
    }

    @Override
    public List<TakeOut> getListByKeywordAndPage(String keyword, int page) {
        return takeOutDAO.getListByKeywordAndPage(keyword, page);
    }

    @Override
    public List<TakeOut> getListByPhone(String phone) {
        return takeOutDAO.getListByPhone(phone);
    }

    @Override
    public int getCountBySenderPhone(String phone) {
        return takeOutDAO.getCountBySenderPhone(phone);
    }

    @Override
    public int getCountByBlock(String block) {
        return takeOutDAO.getCountByBlock(block);
    }

    @Override
    public int getCountByBlockAndGroup(String block, String group) {
        return takeOutDAO.getCountByBlockAndGroup(block, group);
    }

    @Override
    public int getCount() {
        return takeOutDAO.getCount();
    }
}
