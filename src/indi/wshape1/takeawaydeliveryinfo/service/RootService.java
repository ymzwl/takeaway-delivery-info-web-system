package indi.wshape1.takeawaydeliveryinfo.service;

import indi.wshape1.takeawaydeliveryinfo.dao.RootDAO;

import java.sql.SQLException;

/**
 * @author Wshape1
 * @create 2022-11-16 19:37
 */

public class RootService {

    private final RootDAO rootDAO = new RootDAO();

    public String getRootUser() {
        return rootDAO.getRootUser();
    }

    public String getRootPassword() {
        return rootDAO.getRootPassword();
    }

    public boolean updateRootPassword(String password) throws SQLException {
        rootDAO.updateRootPassword(password);
        return true;
    }

}
