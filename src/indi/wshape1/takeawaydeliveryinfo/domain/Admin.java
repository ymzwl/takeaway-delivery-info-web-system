package indi.wshape1.takeawaydeliveryinfo.domain;

/**
 * @author Wshape1
 * @create 2022-11-07 18:50
 */

public class Admin {
    private String ad_name = null;
    private String phone;
    private String password;
    private String block;

    public Admin() {
    }

    public Admin(String phone, String password, String block) {
        this.phone = phone;
        this.password = password;
        this.block = block;
    }

    public String getName() {
        return ad_name;
    }

    public void setName(String name) {
        this.ad_name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }
}
