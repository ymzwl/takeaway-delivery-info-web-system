package indi.wshape1.takeawaydeliveryinfo.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * @author Wshape1
 * @create 2022-11-09 22:25
 */

public interface BaseBAO<T> {

    Connection getConnection();

    void close(Connection connection);

    void close(Statement statement);

    void close(ResultSet resultSet);

    /**
     * @param sql    SQL语句
     * @param params 传入的参数
     * @return 执行查询并返回查询的结果List
     */
    List<T> executeQuery(String sql, Object... params);

    /**
     * @param sql    SQL语句
     * @param params 传入的参数
     * @return 执行更新数据库数据并返回影响的行数
     */
    int executeUpdate(String sql, Object... params) throws SQLException;

    /**
     * 可用于查询数量等
     *
     * @param sql    SQL语句
     * @param params 传入的参数
     * @return 返回Object[]储存查询数据的第一行
     */
    Object[] executeComplexQuery(String sql, Object... params);


}
