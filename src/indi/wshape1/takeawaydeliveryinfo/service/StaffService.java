package indi.wshape1.takeawaydeliveryinfo.service;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Wshape1
 * @create 2022-11-11 12:52
 */

public interface StaffService<T> {
    T getByPhone(String phone);

    boolean add(T t);

    boolean delByPhone(String phone) throws SQLException;

    boolean update(T t, String oldPhone) throws SQLException;

    List<T> getList();

    List<T> getListByPage(int page);

    List<T> getListByKeywordAndPage(String keyword, int page);

    List<T> getListByKeyword(String keyword);

    int getCount();

    int getCountByKeyword(String keyword);

    int getPageCount();

    int getPageCountByKeyword(String keyword);

    List<T> getListByBlockByKeywordAndPage(String block, String keyword, int page);
}
