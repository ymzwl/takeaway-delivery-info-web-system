package indi.wshape1.takeawaydeliveryinfo.service.impl;

import indi.wshape1.takeawaydeliveryinfo.dao.AdminDAO;
import indi.wshape1.takeawaydeliveryinfo.dao.impl.AdminDAOImpl;
import indi.wshape1.takeawaydeliveryinfo.domain.Admin;
import indi.wshape1.takeawaydeliveryinfo.service.AdminService;
import indi.wshape1.takeawaydeliveryinfo.service.BlockService;
import indi.wshape1.takeawaydeliveryinfo.util.StringUtil;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Wshape1
 * @create 2022-11-11 12:49
 */

public class AdminServiceImpl implements AdminService {

    private final AdminDAO adminDAO = new AdminDAOImpl();


    @Override
    public Admin getByPhone(String phone) {
        if (StringUtil.checkPhone(phone)) {
            return adminDAO.getByPhone(phone);
        }
        return null;
    }

    @Override
    public boolean add(Admin admin) {
        // need check admin.block
        BlockService blockService = new BlockServiceImpl();
        if (blockService.getByName(admin.getBlock()) != null) {
            if (getByPhone(admin.getPhone()) == null) {
                adminDAO.add(admin);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean delByPhone(String phone) throws SQLException {
        if (getByPhone(phone) != null) {
            adminDAO.delByPhone(phone);
            return true;
        }
        return false;
    }

    @Override
    public boolean update(Admin admin, String oldPhone) throws SQLException {
        // need check admin.block
        BlockService blockService = new BlockServiceImpl();
        if (blockService.getByName(admin.getBlock()) != null) {

            if (getByPhone(oldPhone) != null) {
                if (oldPhone.equals(admin.getPhone()) || getByPhone(admin.getPhone()) == null) {
                    adminDAO.update(admin, oldPhone);
                    return true;
                }

            }

        }
        return false;
    }

    @Override
    public List<Admin> getList() {
        return adminDAO.getList();
    }

    @Override
    public List<Admin> getListByPage(int page) {
        return adminDAO.getListByPage(page);
    }

    @Override
    public List<Admin> getListByKeywordAndPage(String keyword, int page) {
        return adminDAO.getListByKeywordAndPage(keyword, page);
    }

    @Override
    public List<Admin> getListByKeyword(String keyword) {
        return adminDAO.getListByKeyword(keyword);
    }

    @Override
    public int getCount() {
        return adminDAO.getCount();
    }

    @Override
    public int getCountByKeyword(String keyword) {
        return adminDAO.getCountByKeyword(keyword);
    }

    @Override
    public int getPageCount() {
        return (getCount() - 1 + adminDAO.getCountPerPage()) / adminDAO.getCountPerPage();
    }

    @Override
    public int getPageCountByKeyword(String keyword) {
        return (getCountByKeyword(keyword) - 1 + adminDAO.getCountPerPage()) / adminDAO.getCountPerPage();
    }

    @Override
    public List<Admin> getListByBlockByKeywordAndPage(String block, String keyword, int page) {
        return adminDAO.getListByBlockByKeywordAndPage(block, keyword, page);
    }

    @Override
    public List<Admin> getListByBlock(String block) {
        return adminDAO.getListByBlock(block);
    }

    @Override
    public boolean freeAdminByBlock(String block) throws SQLException {
        return adminDAO.batchUpdateByBlock(new String[]{"block"}, "null", block);
    }
}
