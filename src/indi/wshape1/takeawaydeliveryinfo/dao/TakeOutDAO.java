package indi.wshape1.takeawaydeliveryinfo.dao;

import indi.wshape1.takeawaydeliveryinfo.domain.TakeOut;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Wshape1
 * @create 2022-11-16 14:59
 */

public interface TakeOutDAO {

    int countPerPage = 200;

    default int getCountPerPage() {
        return countPerPage;
    }

    List<TakeOut> getListByGroupAndBlockByPage(String group, String block, int page);

    List<TakeOut> getListByGroupAndBlockByKeywordAndPage(String group, String block, String keyword, int page);

    int getPageCountByGroupAndBlock(String group, String block, String keyword);

    List<TakeOut> getListByBlockByPage(String block, int page);

    List<TakeOut> getListByBlockByKeywordAndPage(String block, String keyword, int page);

    int getPageCountByBlock(String block, String keyword);

    List<TakeOut> getListBySenderPhoneByPage(String se_phone, int page);

    List<TakeOut> getListBySenderPhoneByKeywordAndPage(String se_phone, String keyword, int page);

    int getPageCountBySenderPhone(String se_phone, String keyword);

    List<TakeOut> getListByPhonePage(String phone, int page);

    int getPageCountByPhone(String phone);

    void updateInfo(TakeOut takeOut) throws SQLException;

    void deleteInfo(int id) throws SQLException;

    void addInfo(TakeOut takeOut);

    TakeOut getById(int id);

    void deleteByBlockGroup(String block, String group) throws SQLException;

    void deleteByBlock(String block) throws SQLException;

    List<TakeOut> getListBetweenByPage(String start, String end, int page);

    List<TakeOut> getListBetweenByKeywordAndPage(String start, String end, String keyword, int page);

    int getPageCountBetween(String start, String end, String keyword);

    List<TakeOut> getListByKeywordAndPage(String keyword, int page);

    List<TakeOut> getListByPhone(String phone);

    int getCountBySenderPhone(String phone);

    int getCountByBlock(String block);

    int getCountByBlockAndGroup(String block, String group);

    int getCount();

}
