package indi.wshape1.takeawaydeliveryinfo.service;

import indi.wshape1.takeawaydeliveryinfo.domain.Sender;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Wshape1
 * @create 2022-11-12 10:16
 */

public interface SenderService extends StaffService<Sender> {

    List<Sender> getListByGroup(String group);

    List<Sender> getListByBlock(String block);

    List<Sender> getListByGroupAndBlock(String group, String block);

    boolean freeSenderByGroup(String group) throws SQLException;

    boolean freeSenderByBlock(String block) throws SQLException;


    List<Sender> getListByGroupAndBlockByKeywordAndPage(String selectedGroup, String block, String keyword, int page);

    int getCountByBlock(String block);

}
