package indi.wshape1.takeawaydeliveryinfo.dao;

import indi.wshape1.takeawaydeliveryinfo.domain.Block;

/**
 * @author Wshape1
 * @create 2022-11-11 16:34
 */

public interface BlockDAO extends RangeDAO<Block> {

    int countPerPage = 100;

    default int getCountPerPage() {
        return countPerPage;
    }


}
