function login() {
    // alert("进入login()");
    let user = document.getElementById("user").value;
    let pwd = document.getElementById("pwd").value;
    let type;
    let inElems = document.getElementsByName("type");
    for (let i = 0; i < inElems.length; i++) {
        if (inElems[i].checked) {
            type = inElems[i].value;
            break;
        }
    }


    if (type !== "root") {
        let reg = new RegExp("^\\d{11}$");
        if (!reg.test(user)) {
            document.getElementById("loginHint").innerText = "请正确输入账号(11位数字)";
            return;
        }
    }

    if (!(new RegExp("[a-zA-Z\\d#*+._-]{6,18}")).test(pwd)) {

        document.getElementById("loginHint").innerText = "请正确输入密码(6-18位, 允许特殊符号#*+._-)";
        return;
    }

    let inputVerifyCode = document.getElementById("kaptcha").value;
    if (!(new RegExp("^\[a-zA-Z0-9]{4}$")).test(inputVerifyCode)) {
        document.getElementById("loginHint").innerText = "请输入正确的验证码";
        return;
    }

    let noLogin = "false";
    if(document.getElementsByName("noLogin")[0].checked)
        noLogin = "true";

    document.getElementById("loginHint").innerText = "正在等待验证...";

    let xmlhttp;
    if (window.XMLHttpRequest) {
        //  IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
        xmlhttp = new XMLHttpRequest();
    } else {
        // IE6, IE5 浏览器执行代码
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === XMLHttpRequest.DONE && xmlhttp.status === 200) {
            // alert("服务器获取成功");
            let returnState = parseInt(xmlhttp.responseText);
            // alert(returnState);
            if (returnState === -1) {
                document.getElementById("loginHint").innerText = "验证码错误！";
                changeVerifyCode(document.getElementById("kaptcha_img"));
            } else if (returnState === 0) {
                // alert("账号或密码错误");
                document.getElementById("loginHint").innerText = "账号或密码错误";
            } else if (returnState === 1) {
                // alert("此账号已登陆");
                document.getElementById("loginHint").innerText = "此账号已登录";
            } else if (returnState === 2) {
                // alert("登录成功");
                document.getElementById("loginHint").innerText = "登录成功，请等待跳转...";
                window.location.replace("page.html");
            }

        }
    }
    xmlhttp.open("POST", "login", false);
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xmlhttp.send("inputVerifyCode=" + inputVerifyCode.toLowerCase() + "&type=" + type + "&user=" + user + "&pwd=" + pwd + "&noLogin=" + noLogin);
}