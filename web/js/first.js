window.onload = function () {
    // alert("onpageshow()")
    let xmlhttp;
    if (window.XMLHttpRequest) {
        //  IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
        xmlhttp = new XMLHttpRequest();
    } else {
        // IE6, IE5 浏览器执行代码
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === XMLHttpRequest.DONE && xmlhttp.status === 200) {
            // alert("服务器获取成功");
            let returnState = parseInt(xmlhttp.responseText);
            let path = window.location.pathname.split("/");
            let page = path[path.length - 1];
            if (returnState === 0) {
                if (page !== "login.html")
                    window.location.replace("login.html");
            } else if (returnState === 1) {
                if (page !== "page.html" && page !== "change.html" && page !== "more.html")
                    window.location.replace("page.html");
            }
        }
    }
    xmlhttp.open("GET", "onload", true);
    xmlhttp.send();
}