package indi.wshape1.takeawaydeliveryinfo.servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

/**
 * @author Wshape1
 * @create 2022-11-19 10:59
 */

@WebServlet(name = "QuitServlet", value = {"/quit"})
public class QuitServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getServletContext().removeAttribute((String) req.getSession().getAttribute("user"));
        req.getSession().removeAttribute("user");

        Cookie noLogin = new Cookie("noLogin", null);
        noLogin.setMaxAge(0);
        noLogin.setPath("/");
        resp.addCookie(noLogin);

        req.getSession().invalidate();

        resp.sendRedirect("login.html");
    }
}
