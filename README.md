# TakeawayDeliveryInfoWebSystem

#### 前言

- 外卖配送信息web系统  
- 采用HTML+CSS+JS+Ajax+Servlet+MySQL进行粗略简单的实现
- 此项目只是为了应付期末作业而作，其代码内容结构混乱，可读性差
- 页面由[SpringHHYM](https://gitee.com/YYYyugen)进行美化
- 此项目存在少数已知BUG
  - Session过期后不会退出，仍可以操作导致报错
  - ...


**外卖配送信息系统(Web)**

 

目录

[简介... 1](#_Toc121764204)

[成员及分工... 2](#_Toc121764205)

[MySQL数据库结构：... 2](#_Toc121764206)

[项目文件介绍... 5](#_Toc121764207)

[详细介绍... 5](#_Toc121764208)

[1.  初始页面（登录页面）... 5](#_Toc121764209)

[2.  客户查询外卖信息界面... 5](#_Toc121764210)

[3.  各角色后台界面展示... 6](#_Toc121764211)

[4.  修改资料界面... 8](#_Toc121764212)

[5.  更多信息页面... 11](#_Toc121764213)

[6.  后台查询功能... 13](#_Toc121764214)

[7.  送达和删除... 14](#_Toc121764215)

 

## 简介

该系统可以通过手机号码将客户外卖的信息录入，告知客户外卖的配送情况（通过某种方式，如手机短信）。客户可以通过访问该系统网站来查询外卖信息。可以通过‘区域’和‘组别’对外卖进行分类，其中区域范围大于组别且包含组别。主要角色有超级管理员（有且只有一个，拥有最高权限）、管理员（管理区域及其包含的组别，可多个管理员管理同一区域，权限次于超级管理员，可以创建组别和配送员），配送员（仅对外卖信息进行录入、删除、修改送达状态），客户（查询外卖信息）。

主要技术栈：MySQL+Servlet+Ajax+HTML+CSS+JS

开发工具及环境

- JDK 17  
- IDEA 2022  
- Tomcat 10  
- MySQL 8.0.12  
- Navicat Premium 16  

## MySQL数据库结构：

   表：`root`, `admin`, `sender`, `block`, `group`, `takeout_info`

ER图如下：（🔑主键，◆索引）![img](img/er.jpg)

![img](img/tables.png)详细表信息：

储存过程如下：

![img](img/produce.jpg)

调用于信息查询时。

**（详细查看“[MySQL数据库结构.xlsx](MySQL数据库结构.xlsx)”）**

## 项目文件介绍

![img](img/filelist.jpg)

## 详细介绍

\1.  进入网站初始页面为登录页面 – login.html

![img](img/login_page.jpg)

 

\2.  客户查询外卖点击“点击查询外卖信息”链接进入 – query.html

![img](img/query_page.jpg)

输入手机号码和验证码，从数据库获取信息查询：

![img](img/query_result1.jpg)

查询语句如下：

 

```java
public List<TakeOut> getListByPhone(String phone) {
   return executeQuery("SELECT `id`, `name`, `phone`, `time`, `status`, `group`, `block`, `sender_phone`, `detail` " +
           "FROM `takeout_info` WHERE `phone` = ?",
       phone);
 }
```

 

查询通过服务器Servlet对数据进行包装成JSON并发送给Ajax。

![img](img\query_code1.jpg)

\3.  各角色后台界面展示（功能展示在下文） – page.html

超级管理员默认账号为root，密码默认为rootroot。管理员和配送员只能由超级管理员创建，管理员可以创建配送员。

\1)   超级管理员

![img](img/page_root.jpg)

\2)   管理员

![img](img/page_admin.jpg)

\3)   配送员

![img](img/page_sender.jpg)

\4.  修改资料界面（以管理员和配送员为例） - change.html

![img](img/change_name1.jpg)

修改资料功能修改数据库数据实现数据更改，以下是实现代码：

（下一图以修改配送员为例）

![img](img/change_code0.jpg)

![img](img/change_code1.jpg)

![img](img/change_code2.jpg)

 

修改资料功能对数据库访问修改时会开启事务（下文‘**第7送达和删除**’也会开启事务，不在赘述）：

通过Filter过滤客户端发送的Servlet请求，如修改，添加，外卖送达，删除。

在进行数据库操作过程，所有异常都会抛出，以便Filter捕获异常回滚事务，以下是MyFilter代码：

![img](img/filter_code.jpg)

\5.  更多信息页面，如统计，添加，已超级管理员后台为例： - more.html

![img](img/more_root.jpg)

数据统计通过Ajax发送GET请求给服务端，获取返回值并局部更新显示数据：

![img](img/more_code1.jpg)

添加功能代码如下：

（以添加外卖信息为例）

![img](img/more_add1.jpg)

![img](img/more_add2.jpg)

 

\6.  后台查询功能（查询外卖，管理员，配送员，区域，组别；以超级管理员为例） – page.html

![img](img/page_bar.jpg)

可操作选项有4项，第一项‘区域’选择要查询的区域，不选择将查询全部区域，’组别’选项同理。若管理员和配送员无区域和组别分配，则它们将属于null区域和null组别（不可删除）吗，一下查询结果显示不存在正在待分配的配送员：

![img](img/page_null.jpg)

最后一个选项’搜索‘可以搜索关键词（被匹配字段有名称、手机号码、区域、组别、配送状态）：

![img](img/page_search1.jpg)

![img](img/page_search2.jpg)

![img](img/page_search3.jpg)

查询代码如下（以查询外卖为例）：

![img](img/page_code1.jpg)

以上代码使用了储存过程getTakeOutListByKeywordPage，详细查看[MySQL数据库结构.xlxs](MySQL数据库结构.xlsx)

\7.  送达和删除

送达和删除过程在访问数据库时有开启事务。

 勾选查询表格‘选择’列按钮进行操作，点击垃圾桶图案或者飞机图案进行删除或者确认送达。

删除功能展示如下（以删除配送员为例）：

![img](img/page_delete.jpg)

![img](img/page_code2.jpg)

送达功能展示如下：

 

![img](img/page_send.jpg)

![img](img/page_sent.jpg)

修改送达代码同**第4修改资料**