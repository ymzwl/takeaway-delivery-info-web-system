package indi.wshape1.takeawaydeliveryinfo.listener;

import indi.wshape1.takeawaydeliveryinfo.util.ConnectionUtil;
import indi.wshape1.takeawaydeliveryinfo.util.StringUtil;
import jakarta.servlet.annotation.WebListener;
import jakarta.servlet.http.HttpSessionEvent;
import jakarta.servlet.http.HttpSessionListener;

import java.sql.SQLException;

/**
 * @author Wshape1
 * @create 2022-11-29 21:17
 */

@WebListener
public class MySessionListener implements HttpSessionListener {
    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        String user = (String) se.getSession().getAttribute("user");
        if (!StringUtil.isEmpty(user)) {
            se.getSession().getServletContext().removeAttribute(user);
            se.getSession().removeAttribute("user");
        }

        try {
            ConnectionUtil.closeConnection();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }
}
