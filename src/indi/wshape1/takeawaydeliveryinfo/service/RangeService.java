package indi.wshape1.takeawaydeliveryinfo.service;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Wshape1
 * @create 2022-11-12 10:29
 */

public interface RangeService<T> {
    T getByName(String name);

    boolean add(T t);

    boolean delByName(String name) throws SQLException;

    boolean update(T newT, String oldName) throws SQLException;

    List<T> getList();

    List<T> getListByPage(int page);

    List<T> getListByKeywordAndPage(String keyword, int page);

    List<T> getListByKeyword(String keyword);

    int getCount();

    int getCountByKeyword(String keyword);

    int getPageCount();

    int getPageCountByKeyword(String keyword);

}
