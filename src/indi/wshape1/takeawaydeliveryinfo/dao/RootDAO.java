package indi.wshape1.takeawaydeliveryinfo.dao;

import indi.wshape1.takeawaydeliveryinfo.dao.impl.BaseDAOImpl;

import java.sql.SQLException;

/**
 * @author Wshape1
 * @create 2022-11-13 17:51
 */

public class RootDAO extends BaseDAOImpl<Object> {
    public String getRootUser() {
        return (String) executeComplexQuery("SELECT `name` FROM `root` LIMIT 1")[0];
    }

    public String getRootPassword() {
        return (String) executeComplexQuery("SELECT `password` FROM `root` LIMIT 1")[0];
    }

    public void updateRootPassword(String password) throws SQLException {
        executeUpdate("UPDATE `root` SET `password` = ? WHERE `name` = ?", password, getRootUser());
    }

}
