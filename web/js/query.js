function query() {
    let phone = document.getElementById("queryPhone").value;
    if (!(new RegExp("^\\d{11}$")).test(phone)) {
        document.getElementById("queryHint").innerText = "请正确输入手机号码";
        return;
    }

    let inputVerifyCode = document.getElementById("kaptcha").value;
    if (!(new RegExp("^\[a-zA-Z0-9]{4}$")).test(inputVerifyCode)) {
        document.getElementById("queryHint").innerText = "请输入正确的验证码";
        return;
    }

    let xmlhttp;
    if (window.XMLHttpRequest) {
        //  IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
        xmlhttp = new XMLHttpRequest();
    } else {
        // IE6, IE5 浏览器执行代码
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === XMLHttpRequest.DONE && xmlhttp.status === 200) {

            if (xmlhttp.responseText === "e") {
                document.getElementById("queryHint").innerText = "验证码错误！";
                changeVerifyCode(document.getElementById("kaptcha_img"));
                return;
            }

            document.getElementById("queryHint").innerText = "查询成功！";
            document.getElementById("queriedPhone").innerText = "收货号码: " + phone;

            let jsonObj = JSON.parse(xmlhttp.responseText);


            //      删除原有列表数据
            // trs 是动态的
            let trs = document.getElementsByTagName("table").item(0).getElementsByTagName("tr");

            for (let i = 0; i < trs.length;) {
                // console.log("i = " + i);
                if (trs[i].getAttribute("class") !== "text-center") {
                    trs[i].remove();
                } else {
                    i++;
                }
            }

            //

            //  列表数据插入
            let str = "<tr id=\"queryData\" class=\"text-center\"></tr>";
            for (let i = 0; i < jsonObj.length; i++) {
                str += "<tr>";
                for(let val of Object.values(jsonObj[i])) {
                    str += "<td>" + val + "</td>";
                }
                str += "</tr>";
            }
            document.getElementById("queryData").outerHTML = str;
            //

        }
    }
    xmlhttp.open("GET", "query?inputVerifyCode=" + inputVerifyCode.toLowerCase() + "&phone=" + phone, true);
    xmlhttp.send();

}

window.changeVerifyCode = function (img) {
    img.src = "/kaptcha?" + Math.floor(Math.random() * 100);
}