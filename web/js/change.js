function changeFunc(func) {

    let a = "";
    let b = "";

    if (func === 0) {
        b = "none";
        setPreName();
    } else if (func === 1) {
        a = "none";
    }
    cleanInput();
    document.getElementById("changeInfo").style.display = a;
    document.getElementById("changePassword").style.display = b;
}

function cleanInput() {
    document.getElementById("name").value = "";
    document.getElementById("oldPwd").value = "";
    document.getElementById("newPwd").value = "";
    document.getElementById("againPwd").value = "";
    document.getElementById("changeHint").innerText = "";
}

function setPreName() {
    let xmlhttp;
    if (window.XMLHttpRequest) {
        //  IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
        xmlhttp = new XMLHttpRequest();
    } else {
        // IE6, IE5 浏览器执行代码
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === XMLHttpRequest.DONE && xmlhttp.status === 200) {
            let info = JSON.parse(xmlhttp.responseText)[0];

            document.getElementById("name").value = info["name"];

            if ("root" === info["type"]) {
                document.getElementById("changeInfo").style.display = "none";
                document.getElementById("changeInfoTag").style.display = "none";
                document.getElementById("changePassword").style.display = "";
            }

        }
    }
    xmlhttp.open("GET", "change?onlyGetName=true", true);
    xmlhttp.send();
}

setPreName();

function changeInfo() {

    let name = document.getElementById("name").value;

    if (!(new RegExp("^[\u4E00-\u9FA5A-Za-z0-9_]+$")).test(name)) {
        document.getElementById("changeHint").innerText = "名字不应该包含特殊字符和空格";
        return;
    }

    if (name.length <= 0 || name.length > 20) {
        document.getElementById("changeHint").innerText = "名字长度应在1至20";
        return;
    }

    let xmlhttp;
    if (window.XMLHttpRequest) {
        //  IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
        xmlhttp = new XMLHttpRequest();
    } else {
        // IE6, IE5 浏览器执行代码
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === XMLHttpRequest.DONE && xmlhttp.status === 200) {
            if (xmlhttp.responseText === "1")
                document.getElementById("changeHint").innerText = "修改成功";
            else
                document.getElementById("changeHint").innerText = "出现未知错误";

        }
    };

    xmlhttp.open("POST", "change", true);
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xmlhttp.send("func=changeInfo&name=" + name);

}


function changePassword() {

    let oldPwd = document.getElementById("oldPwd").value;
    let newPwd = document.getElementById("newPwd").value;
    let againPwd = document.getElementById("againPwd").value;

    let reg = new RegExp("[a-zA-Z\\d#*+._-]{6,18}");

    if (!reg.test(oldPwd)) {
        document.getElementById("changeHint").innerText = "请正确输入旧密码(6-18位, 允许特殊符号#*+._-)";
        return;
    }
    if (!reg.test(newPwd)) {
        document.getElementById("changeHint").innerText = "请正确输入新密码(6-18位, 允许特殊符号#*+._-)";
        return;
    }
    if (!reg.test(againPwd)) {
        document.getElementById("changeHint").innerText = "请正确输入第二次新密码(6-18位, 允许特殊符号#*+._-)";
        return;
    }

    if (newPwd !== againPwd) {
        document.getElementById("changeHint").innerText = "您输入的第一次新密码与第二次新密码不一致";
        return;
    }

    if (newPwd === oldPwd) {
        document.getElementById("changeHint").innerText = "您输入的新密码与旧密码一致，无需修改";
        return;
    }

    let xmlhttp;
    if (window.XMLHttpRequest) {
        //  IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
        xmlhttp = new XMLHttpRequest();
    } else {
        // IE6, IE5 浏览器执行代码
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === XMLHttpRequest.DONE && xmlhttp.status === 200) {
            let returnState = parseInt(xmlhttp.responseText);
            if (returnState === 1) {
                alert("修改密码成功");
                window.location.replace("quit");
            }else if(returnState === 2) {
                document.getElementById("changeHint").innerText = "旧密码输入错误";
            } else if(returnState === 0) {
                document.getElementById("changeHint").innerText = "出现未知错误";
            }

        }
    };

    xmlhttp.open("POST", "change", true);
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xmlhttp.send("func=changePassword&oldPwd=" + oldPwd + "&newPwd=" + newPwd);

}
