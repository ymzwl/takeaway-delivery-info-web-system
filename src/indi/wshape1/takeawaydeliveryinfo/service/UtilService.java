package indi.wshape1.takeawaydeliveryinfo.service;

import indi.wshape1.takeawaydeliveryinfo.domain.Admin;
import indi.wshape1.takeawaydeliveryinfo.domain.Sender;
import indi.wshape1.takeawaydeliveryinfo.service.impl.*;

/**
 * @author Wshape1
 * @create 2022-11-19 9:48
 */

public class UtilService {

    public static final SenderService senderService = new SenderServiceImpl();
    public static final AdminService adminService = new AdminServiceImpl();
    public static final RootService rootService = new RootService();
    public static final BlockService blockService = new BlockServiceImpl();
    public static final GroupService groupService = new GroupServiceImpl();
    public static final TakeOutService takeOutService = new TakeOutServiceImpl();

    public static boolean loginSender(String user, String pwd) {

        Sender sender = senderService.getByPhone(user);

        return sender != null && pwd.equals(sender.getPassword());
    }

    public static boolean loginAdmin(String user, String pwd) {

        Admin admin = adminService.getByPhone(user);

        return admin != null && pwd.equals(admin.getPassword());
    }

    public static boolean loginRoot(String user, String pwd) {

        return user.equals(rootService.getRootUser()) && pwd.equals(rootService.getRootPassword());
    }

    public static String getPersonName(String phone, String type) {
        String name = "root";
        if ("sender".equals(type)) {
            name = senderService.getByPhone(phone).getName();
        } else if ("admin".equals(type)) {
            name = adminService.getByPhone(phone).getName();
        }

        return name;
    }

}
