package indi.wshape1.takeawaydeliveryinfo.domain;

/**
 * @author Wshape1
 * @create 2022-11-07 18:50
 */

public class Sender {
    private String se_name = null;
    private String phone;
    private String password;
    private String block;
    private String group;

    public Sender() {
    }

    public Sender(String phone, String password, String block, String group) {
        this.phone = phone;
        this.password = password;
        this.block = block;
        this.group = group;
    }

    public String getName() {
        return se_name;
    }

    public void setName(String name) {
        this.se_name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }
}
