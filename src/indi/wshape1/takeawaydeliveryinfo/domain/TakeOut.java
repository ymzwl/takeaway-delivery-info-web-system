package indi.wshape1.takeawaydeliveryinfo.domain;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Wshape1
 * @create 2022-11-07 18:50
 */

public class TakeOut {

    private Integer id = null;

    private String block;

    private String name = null;
    private String phone;
    private String sender_phone;
    private Date time = new Date();
    private String group;
    private Status status = Status.SENDING;

    private String detail = null;

    public TakeOut() {
    }

    public TakeOut(String block, String group, String phone, String sender_phone) {
        this.block = block;
        this.phone = phone;
        this.sender_phone = sender_phone;
        this.group = group;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSender_phone() {
        return sender_phone;
    }

    public void setSender_phone(String sender_phone) {
        this.sender_phone = sender_phone;
    }

    public Date getTime() {
        return time;
    }

    public String getTimeToString() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(time);
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public void setTimeByString(String yyyyMMddHHmmss) {
        try {
            this.time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(yyyyMMddHHmmss);
        } catch (ParseException e) {
            throw new RuntimeException();
        }
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }
}
