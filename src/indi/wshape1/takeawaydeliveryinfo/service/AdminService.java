package indi.wshape1.takeawaydeliveryinfo.service;

import indi.wshape1.takeawaydeliveryinfo.domain.Admin;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Wshape1
 * @create 2022-11-11 12:49
 */

public interface AdminService extends StaffService<Admin> {

    List<Admin> getListByBlock(String block);

    boolean freeAdminByBlock(String block) throws SQLException;

}
