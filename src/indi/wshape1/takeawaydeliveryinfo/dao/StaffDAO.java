package indi.wshape1.takeawaydeliveryinfo.dao;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Wshape1
 * @create 2022-11-11 11:02
 */

public interface StaffDAO<T> extends BaseBAO<T> {

    T getByPhone(String phone);

    void add(T t);

    void delByPhone(String phone) throws SQLException;

    void update(T newT, String oldPhone) throws SQLException;

    List<T> getList();

    List<T> getListByPage(int page);

    List<T> getListByKeywordAndPage(String keyword, int page);

    List<T> getListByKeyword(String keyword);

    int getCount();

    int getCountByKeyword(String keyword);

    List<T> getListByBlockByKeywordAndPage(String block, String keyword, int page);
}
