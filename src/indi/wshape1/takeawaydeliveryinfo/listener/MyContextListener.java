package indi.wshape1.takeawaydeliveryinfo.listener;

import com.alibaba.druid.pool.DruidDataSource;
import com.mysql.cj.jdbc.AbandonedConnectionCleanupThread;
import indi.wshape1.takeawaydeliveryinfo.util.ConnectionUtil;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import jakarta.servlet.annotation.WebListener;

import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author Wshape1
 * @create 2022-11-22 21:09
 * @description 监听ContextServlet
 */

@WebListener
public class MyContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent arg0) {
        System.out.println("webService start");
    }

    @Override
    public void contextDestroyed(ServletContextEvent arg0) {
        ((DruidDataSource) ConnectionUtil.getDataSource()).close();
        try {
            while (DriverManager.getDrivers().hasMoreElements()) {
                DriverManager.deregisterDriver(DriverManager.getDrivers().nextElement());
            }
            AbandonedConnectionCleanupThread.uncheckedShutdown();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

