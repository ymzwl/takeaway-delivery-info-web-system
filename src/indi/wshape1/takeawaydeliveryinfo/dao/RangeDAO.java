package indi.wshape1.takeawaydeliveryinfo.dao;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Wshape1
 * @create 2022-11-11 16:37
 */

public interface RangeDAO<T> extends BaseBAO<T> {

    T getByName(String name);

    void add(T t);

    void delByName(String name) throws SQLException;

    void update(T newT, String oldName) throws SQLException;

    List<T> getList();

    List<T> getListByPage(int page);

    List<T> getListByKeywordAndPage(String keyword, int page);

    List<T> getListByKeyword(String keyword);

    int getCount();

    int getCountByKeyword(String keyword);

}
